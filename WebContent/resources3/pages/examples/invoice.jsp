<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Invoice</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">

<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="index2.jsp" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>Y</b>T</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Yurt</b>Otomasyonu</span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          
          
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="../../dist/img/fatih.jpg" class="user-image" alt="User Image">
              <span class="hidden-xs">Fatih Tüfekçi</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="../../dist/img/fatih.jpg" class="img-circle" alt="User Image">
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <p>
                  Fatih Tüfekçi - Web Developer
                </p>
                <div class="pull-right">
                  <a href="login2.jsp" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>

    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="../../dist/img/fatih.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Fatih Tüfekçi</p>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                  <i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="active treeview menu-open">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span> Dashboard</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="../../../index2.jsp"><i class="fa fa-circle-o"></i> Dashboard</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span> Öğrenci İşlemleri</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="OgrenciController"><i class="fa fa-circle-o"></i> Öğrenci Ekle</a></li>
            <li><a href="resources3/pages/forms/advanced.html"><i class="fa fa-circle-o"></i> Öğrenci Listele</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span>Personel İşlemleri</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="resources3/pages/tables/simple.html"><i class="fa fa-circle-o"></i> Personel Ekle</a></li>
            <li><a href="resources3/pages/tables/data.html"><i class="fa fa-circle-o"></i> Personel Listele</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-table"></i> <span>Ödeme İşlemleri</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="resources3/pages/tables/simple.html"><i class="fa fa-circle-o"></i> Öğrenci Borç</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-folder"></i> <span>Examples</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="resources3/pages/examples/invoice.html"><i class="fa fa-circle-o"></i> Invoice</a></li>
            <li><a href="resources3/pages/examples/login.html"><i class="fa fa-circle-o"></i> Login</a></li>
            <li><a href="resources3/pages/examples/register.html"><i class="fa fa-circle-o"></i> Register</a></li>
            <li><a href="resources3/pages/examples/404.html"><i class="fa fa-circle-o"></i> 404 Error</a></li>
          </ul>
        </li>
        
        <li><a href="https://adminlte.io/docs"><i class="fa fa-book"></i> <span>Duyurular</span></a></li>
        <li><a href="https://adminlte.io/docs"><i class="fa fa-th"></i> <span>Disiplin Cezaları</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	
	<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    
    <div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-6">
						<form class="form-horizontal" action="OgrenciController"
							method="POST">
							<fieldset>

								<!-- Form Name -->
								<legend>Öğrenci Ekle</legend>

								<!-- Text input-->
								<div class="form-group">
									<label class="col-md-4 control-label" for="ad">Ad</label>
									<div class="col-md-4">
										<input id="ad" name="ad" type="text" placeholder=""
											class="form-control input-md">

									</div>
								</div>

								<!-- Text input-->
								<div class="form-group">
									<label class="col-md-4 control-label" for="soyad">Soyad</label>
									<div class="col-md-4">
										<input id="soyad" name="soyad" type="text" placeholder=""
											class="form-control input-md">

									</div>
								</div>

								<!-- Text input-->
								<div class="form-group">
									<label class="col-md-4 control-label" for="tc">TC</label>
									<div class="col-md-4">
										<input id="tc" name="tc" type="text" placeholder=""
											class="form-control input-md">

									</div>
								</div>


								<!-- Text input-->
								<div class="form-group">
									<label class="col-md-4 control-label" for="tel">Telefon</label>
									<div class="col-md-4">
										<input id="tel" name="tel" type="text" placeholder=""
											class="form-control input-md">

									</div>
								</div>

								<!-- Text input-->
								<div class="form-group">
									<label class="col-md-4 control-label" for="velitel">Veli
										Telefon</label>
									<div class="col-md-4">
										<input id="velitel" name="velitel" type="text" placeholder=""
											class="form-control input-md">

									</div>
								</div>

								<div class="form-group">
									<label
										style="font-size: 18px; font-weight: bold; font-family: 'Arial'">Öğrenci
										Sınıfı</label> <select name="kacinciSinif" class="form-control">
										<option value="1">Lise 1</option>
										<option value="2">Lise 2</option>
										<option value="3">Lise 3</option>
										<option value="4">Lise 4</option>
									</select>
								</div>

								<div class="form-group">
									<label
										style="font-size: 18px; font-weight: bold; font-family: 'Arial'">Odalar</label>
									<select name="oda" class="form-control">
										<c:forEach items="${odalar}" var="odaniz">
											<option value="${odaniz.odaNo}"><c:out value="${odaniz.odaKapasite}"/><p> kişilik, </p>
											<c:out value="${odaniz.odaNo}" /><p> no'lu oda</p></option>
										</c:forEach>
									</select>
								</div>


								<!-- Button -->
								<div class="form-group">
									<label class="col-md-4 control-label" for="ekle"></label>
									<div class="col-md-4">
										<input type="submit" class="btn btn-info" value="ogrenciEkle">
									</div>
								</div>

							</fieldset>
						</form>
					</div>
					<div class="col-md-6"></div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<legend>Öğrenci Listesi</legend>
						<div class="table-responsive">
							<table id="example" class="table table-striped table-bordered">
								<thead>
									<tr>
										<th>Ad</th>
										<th>Soyad</th>
										<th>TC</th>
										<th>Telefon</th>
										<th>Veli Telefon</th>
										<th>Kaçıncı Sınıf</th>
										<th>Oda No</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${ogrenciler}" var="ogrenci">
										<tr>
											<td><c:out value="${ogrenci.ad}" /></td>
											<td><c:out value="${ogrenci.soyad}" /></td>
											<td><c:out value="${ogrenci.tc}" /></td>
											<td><c:out value="${ogrenci.telefon}" /></td>
											<td><c:out value="${ogrenci.veliTelefon}" /></td>
											<td><c:out value="${ogrenci.kacinciSinif}" /></td>
											<td><c:out value="${ogrenci.odaNo}" /></td>
											<td><a
												href="OgrenciController?ogrenciId=${ogrenci.ogrenciId}"
												class="btn btn-danger" type="button">Delete</a></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
					<div class="col-md-6"></div>
				</div>
			</div>
		</div>
	</div>
    
	
    <!-- /.content -->
    <div class="clearfix"></div>
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer no-print">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2018-2019 <a href="https://adminlte.io">Yurt Otomasyonu</a>.</strong> All rights reserved
  </footer>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="../../bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>

</body>
</html>