<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">

<title>Hatalı Giriş</title>

<!-- Bootstrap core CSS-->
<link href="../resources2/vendor/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">

<!-- Custom fonts for this template-->
<link href="../resources2/vendor/fontawesome-free/css/all.min.css"
	rel="stylesheet" type="text/css">

<!-- Custom styles for this template-->
<link href="../resources2/css/sb-admin.css" rel="stylesheet">
</head>
<body class="bg-dark">

	<div class="container">
		<div class="card card-login mx-auto mt-5">
			<div class="card-header">LÜTFEN GİRİŞ YAPINIZ..!!</div>
			<div class="card-body">
					<section>
						<ul>
							<li><a href="../admin/login2.jsp">
							<i class="fa fa-adjust"></i> <span>Giriş yapmak için tıklayın</span></a></li>
						</ul>
					</section>
			</div>
		</div>
	</div>

	<!-- Bootstrap core JavaScript-->
	<script src="../resources2/vendor/jquery/jquery.min.js"></script>
	<script src="../resources2/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

	<!-- Core plugin JavaScript-->
	<script src="../resources2/vendor/jquery-easing/jquery.easing.min.js"></script>

</body>
</html>