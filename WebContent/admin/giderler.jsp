<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Giderler</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.7 -->
<link rel="stylesheet"
	href="../resources3/bower_components/bootstrap/dist/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="../resources3/bower_components/font-awesome/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="../resources3/bower_components/Ionicons/css/ionicons.min.css">
<!-- jvectormap -->
<link rel="stylesheet"
	href="../resources3/bower_components/jvectormap/jquery-jvectormap.css">
<!-- Theme style -->
<link rel="stylesheet" href="../resources3/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet"
	href="../resources3/dist/css/skins/_all-skins.min.css">



<!-- Google Font -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">

	<div class="wrapper">

		<header class="main-header">

			<!-- Logo -->
			<a href="index2.jsp" class="logo"> <!-- mini logo for sidebar mini 50x50 pixels -->
				<span class="logo-mini"><b>Y</b>O</span> <!-- logo for regular state and mobile devices -->
				<span class="logo-lg"><b>Yurt</b>Otomasyonu</span>
			</a>

			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top">
				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="push-menu"
					role="button"> <span class="sr-only">Toggle navigation</span>
				</a>
				<!-- Navbar Right Menu -->
				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">


						<!-- User Account: style can be found in dropdown.less -->
						<li class="dropdown user user-menu"><a href="#"
							class="dropdown-toggle" data-toggle="dropdown"> <img
								src="../resources3/dist/img/fatih.jpg" class="user-image"
								alt="User Image"> <span class="hidden-xs"><c:out
										value="${user.ad}" /> <c:out value="${user.soyad}" /></span>
						</a>
							<ul class="dropdown-menu">
								<!-- User image -->
								<li class="user-header"><img
									src="../resources3/dist/img/fatih.jpg" class="img-circle"
									alt="User Image"></li>
								<!-- Menu Footer-->
								<li class="user-footer">
									<p>
										<span><c:out value="${user.ad}" /> <c:out
												value="${user.soyad}" /> - <c:out value="${user.gorevi}" /></span>
									</p>
									<div class="pull-right">
										<a href="login2.jsp" class="btn btn-default btn-flat">Sign
											out</a>
									</div>
								</li>
							</ul></li>
					</ul>
				</div>

			</nav>
		</header>
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->
				<div class="user-panel">
					<div class="pull-left image">
						<img src="../resources3/dist/img/fatih.jpg" class="img-circle"
							alt="User Image">
					</div>
					<div class="pull-left info">
						<p>
							<c:out value="${user.ad}" />
							<c:out value="${user.soyad}" />
						</p>
					</div>
				</div>
				<!-- search form -->
				<form action="#" method="get" class="sidebar-form">
					<div class="input-group">
						<input type="text" name="q" class="form-control"
							placeholder="Search..."> <span class="input-group-btn">
							<button type="submit" name="search" id="search-btn"
								class="btn btn-flat">
								<i class="fa fa-search"></i>
							</button>
						</span>
					</div>
				</form>
				<!-- /.search form -->
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<ul class="sidebar-menu" data-widget="tree">
					<li class="header">ANA SAYFA</li>

					<li><a href="index2.jsp"><i class="fa fa-adjust"></i> <span>
								Kontrol Paneli</span></a></li>

					<li><a href="OgrenciListele3"><i class="fa fa-book"></i> <span>
								Öğrenci İşlemleri</span></a></li>

					<li><a href="CalisanListController"><i class="fa fa-user"></i>
							<span> Çalışan İşlemleri</span></a></li>

					<li><a href="GiderListController"><i class="fa fa-ticket"></i>
							<span> Giderler</span></a></li>

					<li><a href="DuyuruListeleController"><i
							class="fa fa-bell"></i> <span>Duyurular</span></a></li>

					<li><a href="Disiplin3Controller"><i class="fa fa-th"></i>
							<span>Disiplin Cezaları</span></a></li>
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>


		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">

			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">

						<div class="row">
							<div class="col-md-6">
								<form class="form-horizontal" action="GiderEkleController"
									method="POST">
									<fieldset>

										<!-- Form Name -->
										<legend>Aylık Gider Ekle</legend>

										<!-- Text input-->
										<div class="form-group">
											<label class="col-md-4 control-label" for="elektrik">Elektrik</label>
											<div class="col-md-4">
												<input id="elektrik" name="elektrik" type="number"
													placeholder="" class="form-control input-md">

											</div>
										</div>

										<!-- Text input-->
										<div class="form-group">
											<label class="col-md-4 control-label" for="su">Su</label>
											<div class="col-md-4">
												<input id="su" name="su" type="number" placeholder=""
													class="form-control input-md">

											</div>
										</div>

										<div class="form-group">
											<label class="col-md-4 control-label" for="dogalgaz">Doğalgaz</label>
											<div class="col-md-4">
												<input id="dogalgaz" name="dogalgaz" type="number"
													placeholder="" class="form-control input-md">

											</div>
										</div>


										<div class="form-group">
											<label class="col-md-4 control-label" for="yemekGideri">Yemek
												Gideri</label>
											<div class="col-md-4">
												<input id="yemekGideri" name="yemekGideri" type="number"
													placeholder="" class="form-control input-md">
											</div>
										</div>

										<div class="form-group">
											<label class="col-md-4 control-label" for="giderTarihi">Gider
												Tarihi</label>
											<div class="col-md-4">
												<input id="giderTarihi" name="giderTarihi" type="text"
													placeholder="2019-01-01" class="form-control input-md">
											</div>
										</div>

										<!-- Button -->
										<div class="form-group">
											<label class="col-md-4 control-label" for="giderEkle"></label>
											<div class="col-md-4">
												<input type="submit" class="btn btn-info" value="giderEkle">
											</div>
										</div>

									</fieldset>
								</form>
							</div>
							<div class="col-md-6"></div>
						</div>

						<span style="color: red"><h2>
								<c:out value="${odemeMesaj}" />
							</h2></span>

						<div class="row">
							<div class="col-md-6">
								<legend>Gider Listesi</legend>
								<div class="table-responsive">
									<table id="example" class="table table-striped table-bordered">
										<thead>
											<tr>
												<th>Elektrik Gideri</th>
												<th>Su Gideri</th>
												<th>Doğalgaz Gideri</th>
												<th>Maaş Gideri</th>
												<th>Yemek Gideri</th>
												<th>Gider Tarihi</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach items="${giderler}" var="gider">

												<tr>
													<td><c:out value="${gider.elektrik}" /></td>
													<td><c:out value="${gider.su}" /></td>
													<td><c:out value="${gider.dogalgaz}" /></td>
													<td><c:out value="${gider.maaslar}" /></td>
													<td><c:out value="${gider.yemekGideri}" /></td>
													<td><c:out value="${gider.giderTarihi}" /></td>
													<td><a
														href="OdemeYapsanaController?giderId=${gider.giderId}"
														class="btn btn-success" type="button">Ödeme Yap</a></td>

												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
							</div>
							<div class="col-md-6"></div>
						</div>

					</div>
				</div>
			</div>

			<div class="clearfix"></div>
		</div>
		<!-- /.content-wrapper -->


		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 2.4.0
			</div>
			<strong>Copyright &copy; 2018-2019 <a
				href="https://adminlte.io">İZÜ</a>.
			</strong> Tüm hakları saklıdır
		</footer>
	</div>
	<!-- ./wrapper -->

	<script src="../resources3/bower_components/jquery/dist/jquery.min.js"></script>
	<!-- Bootstrap 3.3.7 -->
	<script
		src="../resources3/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- FastClick -->
	<script src="../resources3/bower_components/fastclick/lib/fastclick.js"></script>
	<!-- AdminLTE App -->

	<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
	<script src="../resources3/dist/js/pages/dashboard2.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="../resources3/dist/js/demo.js"></script>

</body>
</html>