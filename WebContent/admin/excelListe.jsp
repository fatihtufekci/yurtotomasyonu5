<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>

	<legend>Öğrenci Listesi</legend>
	<div class="table-responsive">
		<table id="example" class="table table-striped table-bordered">
			<thead>
				<tr>
					<th>Ad</th>
					<th>Soyad</th>
					<th>TC</th>
					<th>Telefon</th>
					<th>Veli Telefon</th>
					<th>Kaçıncı Sınıf</th>
					<th>Oda No</th>
					<th>Oda Kapasite</th>
					<th>Borç Vadesi</th>
					<th>Aylık Borcu</th>
					<th>Kullanıcı Adı</th>
					<th>Kart ID</th>
				</tr>
			</thead>
			<tbody>
				<%
				response.setContentType("application/ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=ogrList.xls");
				%>
				<c:forEach items="${ogrenciler}" var="ogrenci">
					<tr>
						<td><c:out value="${ogrenci.ad}" /></td>
						<td><c:out value="${ogrenci.soyad}" /></td>
						<td><c:out value="${ogrenci.tc}" /></td>
						<td><c:out value="${ogrenci.telefon}" /></td>
						<td><c:out value="${ogrenci.veliTelefon}" /></td>
						<td><c:out value="${ogrenci.kacinciSinif}" /></td>
						<td><c:out value="${ogrenci.oda.odaNo}" /></td>
						<td><c:out value="${ogrenci.oda.odaKapasite}" /></td>
						<td><c:out value="${ogrenci.borc.borcVade}" /></td>
						<td><c:out value="${ogrenci.borc.borcMiktari}" /></td>
						<td><c:out value="${ogrenci.kullaniciAdi}" /></td>
						<td><c:out value="${ogrenci.kartId}" /></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>

</body>
</html>