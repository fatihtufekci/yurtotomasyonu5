<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>	
    
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Ogrenci</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../resources3/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../resources3/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../resources3/bower_components/Ionicons/css/ionicons.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="../resources3/bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../resources3/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../resources3/dist/css/skins/_all-skins.min.css">
	

	
  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">

<div class="wrapper">

<header class="main-header">

    <!-- Logo -->
    <a href="index2.jsp" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>Y</b>O</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Yurt</b>Otomasyonu</span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          
          
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="../resources3/dist/img/fatih.jpg" class="user-image" alt="User Image">
              <span class="hidden-xs"><c:out value="${user.ad}"/> <c:out value="${user.soyad}"/></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="../resources3/dist/img/fatih.jpg" class="img-circle" alt="User Image">
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <p>
                  <span><c:out value="${user.ad}"/> <c:out value="${user.soyad}"/> - <c:out value="${user.gorevi}"/></span>
                </p>
                <div class="pull-right">
                  <a href="login2.jsp" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>

    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="../resources3/dist/img/fatih.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><c:out value="${user.ad}"/> <c:out value="${user.soyad}"/></p>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                  <i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">ANA SAYFA</li>
        
        <li><a href="index2.jsp"><i class="fa fa-adjust"></i> <span> Kontrol Paneli</span></a></li>
        
        <li><a href="OgrenciListele3"><i class="fa fa-book"></i> <span> Öğrenci İşlemleri</span></a></li>
        
        <li><a href="CalisanListController"><i class="fa fa-user"></i> <span> Çalışan İşlemleri</span></a></li>
		
		<li><a href="GiderListController"><i class="fa fa-ticket"></i> <span> Giderler</span></a></li>
        
        <li><a href="DuyuruListeleController"><i class="fa fa-bell"></i> <span>Duyurular</span></a></li>
        
        <li><a href="Disiplin3Controller"><i class="fa fa-th"></i> <span>Disiplin Cezaları</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    
    <div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-6">
						<!--  action="managerEdit.jsp" -->
							<form class="form-horizontal" action="OgrenciEkle3"
							method="POST">
							<fieldset>
								<input name="id" type="hidden" value="${o.ogrenciId}">
								<!-- Form Name -->
								<legend>Öğrenci Ekle</legend>
								
								<h2 style="color:red">      <c:out value="${hataliMesaj}"/></h2>
								
								<!-- Text input-->
								<div class="form-group">
									<label class="col-md-4 control-label" for="ad">Ad</label>
									<div class="col-md-4">
										<input id="ad" name="ad"  maxlength="30" value="${o.ad}" type="text" placeholder=""
											class="form-control input-md">

									</div>
								</div>

								<!-- Text input-->
								<div class="form-group">
									<label class="col-md-4 control-label" for="soyad">Soyad</label>
									<div class="col-md-4">
										<input id="soyad" name="soyad"  maxlength="40" value="${o.soyad}" type="text" placeholder=""
											class="form-control input-md">

									</div>
								</div>

								<!-- Text input-->
								<div class="form-group">
									<label class="col-md-4 control-label" for="tc">TC</label>
									<div class="col-md-4">
										<input id="tc" name="tc" maxlength="11" value="${o.tc}" type=text placeholder=""
											class="form-control input-md">

									</div>
								</div>


								<!-- Text input-->
								<div class="form-group">
									<label class="col-md-4 control-label" for="tel">Telefon</label>
									<div class="col-md-4">
										<input id="tel" name="tel"  maxlength="11" value="${o.telefon}" type="text" placeholder=""
											class="form-control input-md">

									</div>
								</div>

								<!-- Text input-->
								<div class="form-group">
									<label class="col-md-4 control-label" for="velitel">Veli
										Telefon</label>
									<div class="col-md-4">
										<input id="velitel" name="velitel"  maxlength="11" value="${o.veliTelefon}" type="text" placeholder=""
											class="form-control input-md">

									</div>
								</div>

								<div class="form-group">
									<label
										style="font-size: 18px; font-weight: bold; font-family: 'Arial'">Öğrenci
										Sınıfı</label> <select name="kacinciSinif" class="form-control">
										<option value="${o.kacinciSinif}">Lise ${o.kacinciSinif}</option>
										<option value="1">Lise 1</option>
										<option value="2">Lise 2</option>
										<option value="3">Lise 3</option>
										<option value="4">Lise 4</option>
									</select>
								</div>

								<div class="form-group">
									<label
										style="font-size: 18px; font-weight: bold; font-family: 'Arial'">Odalar</label>
									<select name="oda" class="form-control">
										<option value="${o.oda.odaId}"><c:out value="${o.oda.odaKapasite}"/><p> kişilik, </p>
											<c:out value="${o.oda.odaNo}" /><p> no'lu oda</p></option>
										<c:forEach items="${odalar}" var="odaniz">
											<option value="${odaniz.odaId}"><c:out value="${odaniz.odaKapasite}"/><p> kişilik, </p>
											<c:out value="${odaniz.odaNo}" /><p> no'lu oda</p></option>
										</c:forEach>
									</select>
								</div>
								
								
								<c:if test="${buffer==1}">
								
								<input name="toplamTutar" type="hidden" value="${toplamTutar}">
								
								<span style="font-size: 18px; color:red; font-weight: bold; font-family: 'Arial'">Toplam Borcunuz: </span>
								<label style="font-size: 18px; font-weight: bold; font-family: 'Arial'"><c:out value="${toplamTutar}" /></label>

								
								<div class="form-group">
									<label
										style="font-size: 18px; font-weight: bold; font-family: 'Arial'">Borç Vadesi</label> 
									<select name="borcVade" class="form-control">
										<option value="0">Peşin Ödeme</option>
										<option value="4">4 ay</option>
										<option value="5">5 ay</option>
										<option value="6">6 ay</option>
										<option value="7">7 ay</option>
										<option value="8">8 ay</option>
									</select>
								</div>
								
								<!-- Button -->
								<div class="form-group">
									<label class="col-md-4 control-label" for="ogrenciKayit"></label>
									<div class="col-md-4">
										
										<input type="submit" class="btn btn-info" value="Ögrenci Kayit">
									</div>
								</div>
								
								</c:if>
									
								<c:if test="${buffer==0}">
									<div class="form-group">
									<label class="col-md-4 control-label" for="fiyatGoster"></label>
									<div class="col-md-4">
										<input type="submit" class="btn btn-info" value="Fiyat Goster" formaction="OgrenciListele3">
									</div>
								</div>
								</c:if>
							</fieldset>
						</form>
					</div>
					<div class="col-md-6"></div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<legend>Öğrenci Listesi</legend>
						<div class="table-responsive">
							<table id="example" class="table table-striped table-bordered">
								<thead>
									<tr>
										<th>Ad</th>
										<th>Soyad</th>
										<th>TC</th>
										<th>Telefon</th>
										<th>Veli Telefon</th>
										<th>Kaçıncı Sınıf</th>
										<th>Oda No</th>
										<th>Oda Kapasite</th>
										<th>Borç Vadesi</th>
										<th>Aylık Borcu</th>
										<th>Kullanıcı Adı</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${ogrenciler}" var="ogrenci">
										<tr>
											<td><c:out value="${ogrenci.ad}" /></td>
											<td><c:out value="${ogrenci.soyad}" /></td>
											<td><c:out value="${ogrenci.tc}" /></td>
											<td><c:out value="${ogrenci.telefon}" /></td>
											<td><c:out value="${ogrenci.veliTelefon}" /></td>
											<td><c:out value="${ogrenci.kacinciSinif}" /></td>
											<td><c:out value="${ogrenci.oda.odaNo}" /></td>
											<td><c:out value="${ogrenci.oda.odaKapasite}" /></td>
											<td><c:out value="${ogrenci.borc.borcVade}" /></td>
											<td><c:out value="${ogrenci.borc.borcMiktari}" /></td>
											<td><c:out value="${ogrenci.kullaniciAdi}" /></td>
											<td><a
												href="OgrenciListele3?ogrenciId=${ogrenci.ogrenciId}"
												class="btn btn-danger" type="button">Delete</a></td>
											<td><a
												href="EditOgrenci3?ogrenciId=${ogrenci.ogrenciId}"
												class="btn btn-bitbucket" type="button">Edit</a></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
					<div class="col-md-6"></div>
				</div>
				<br/>
				<br/>
				<div class="row">
					<div class="col-md-6">
						<a href="OgrenciExcel" class="btn btn-success" type="button">Çıktı Al</a>
					</div>
					<div class="col-md-6"></div>
				</div>
			</div>
		</div>
	</div>
    
    <div class="clearfix"></div>
  </div>
  <!-- /.content-wrapper -->
  
  
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2018-2019 <a href="https://adminlte.io">İZÜ</a>.</strong> All rights
    reserved.
  </footer>
</div>
<!-- ./wrapper -->

<script src="../resources3/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../resources3/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="../resources3/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->

<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="../resources3/dist/js/pages/dashboard2.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../resources3/dist/js/demo.js"></script>

</body>
</html>