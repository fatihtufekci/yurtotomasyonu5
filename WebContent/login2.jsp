<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>	
    
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Login</title>

    <!-- Bootstrap core CSS-->
    <link href="resources2/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="resources2/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template-->
    <link href="resources2/css/sb-admin.css" rel="stylesheet">
</head>
<body class="bg-dark">

	    <div class="container">
      <div class="card card-login mx-auto mt-5">
        <div class="card-header">Login</div>
        <div class="card-body">
          <form class="form-signin" method="POST" action="LoginController">
            <div class="form-group">
              <div class="form-label-group">
                <input type="text" id="inputEmail" name="kullaniciAdi" class="form-control" placeholder="Kullanıcı Adı" required="required" autofocus="autofocus">
                <label for="inputEmail">Kullanıcı Adı</label>
              </div>
            </div>
            <div class="form-group">
              <div class="form-label-group">
                <input type="password" name="parola" id="inputPassword" class="form-control" placeholder="Parola" required="required">
                <label for="inputPassword">Parola</label>
              </div>
            </div>
            <button class="btn btn-primary btn-block" type="submit">Giriş</button>
            <input type="hidden" name="action" value="giris">
          </form>
          <div class="text-center">
            <a class="d-block small" href="forgot-password.html">Forgot Password?</a>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="resources2/vendor/jquery/jquery.min.js"></script>
    <script src="resources2/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="resources2/vendor/jquery-easing/jquery.easing.min.js"></script>

</body>
</html>