<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Öğrenci İşlemleri</title>

<script src="resources/js/bootstrap.min.js"></script>
<link href="resources/css/bootstrap.min.css" rel="stylesheet">

</head>
<body>


	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-6">
						<form class="form-horizontal" action="OgrenciController"
							method="POST">
							<fieldset>

								<!-- Form Name -->
								<legend>Öğrenci Ekle</legend>

								<!-- Text input-->
								<div class="form-group">
									<label class="col-md-4 control-label" for="ad">Ad</label>
									<div class="col-md-4">
										<input id="ad" name="ad" type="text" placeholder=""
											class="form-control input-md">

									</div>
								</div>

								<!-- Text input-->
								<div class="form-group">
									<label class="col-md-4 control-label" for="soyad">Soyad</label>
									<div class="col-md-4">
										<input id="soyad" name="soyad" type="text" placeholder=""
											class="form-control input-md">

									</div>
								</div>

								<!-- Text input-->
								<div class="form-group">
									<label class="col-md-4 control-label" for="tc">TC</label>
									<div class="col-md-4">
										<input id="tc" name="tc" type="text" placeholder=""
											class="form-control input-md">

									</div>
								</div>


								<!-- Text input-->
								<div class="form-group">
									<label class="col-md-4 control-label" for="tel">Telefon</label>
									<div class="col-md-4">
										<input id="tel" name="tel" type="text" placeholder=""
											class="form-control input-md">

									</div>
								</div>

								<!-- Text input-->
								<div class="form-group">
									<label class="col-md-4 control-label" for="velitel">Veli
										Telefon</label>
									<div class="col-md-4">
										<input id="velitel" name="velitel" type="text" placeholder=""
											class="form-control input-md">

									</div>
								</div>

								<div class="form-group">
									<label
										style="font-size: 18px; font-weight: bold; font-family: 'Arial'">Öğrenci
										Sınıfı</label> <select name="kacinciSinif" class="form-control">
										<option value="1">Lise 1</option>
										<option value="2">Lise 2</option>
										<option value="3">Lise 3</option>
										<option value="4">Lise 4</option>
									</select>
								</div>

								<div class="form-group">
									<label
										style="font-size: 18px; font-weight: bold; font-family: 'Arial'">Odalar</label>
									<select name="oda" class="form-control">
										<c:forEach items="${odalar}" var="odaniz">
											<option value="${odaniz.odaNo}"><c:out value="${odaniz.odaKapasite}"/><p> kişilik, </p>
											<c:out value="${odaniz.odaNo}" /><p> no'lu oda</p></option>
										</c:forEach>
									</select>
								</div>


								<!-- Button -->
								<div class="form-group">
									<label class="col-md-4 control-label" for="ekle"></label>
									<div class="col-md-4">
										<input type="submit" class="btn btn-info" value="ogrenciEkle">
									</div>
								</div>

							</fieldset>
						</form>
					</div>
					<div class="col-md-6"></div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<legend>Öğrenci Listesi</legend>
						<div class="table-responsive">
							<table id="example" class="table table-striped table-bordered">
								<thead>
									<tr>
										<th>Ad</th>
										<th>Soyad</th>
										<th>TC</th>
										<th>Telefon</th>
										<th>Veli Telefon</th>
										<th>Kaçıncı Sınıf</th>
										<th>Oda No</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${ogrenciler}" var="ogrenci">
										<tr>
											<td><c:out value="${ogrenci.ad}" /></td>
											<td><c:out value="${ogrenci.soyad}" /></td>
											<td><c:out value="${ogrenci.tc}" /></td>
											<td><c:out value="${ogrenci.telefon}" /></td>
											<td><c:out value="${ogrenci.veliTelefon}" /></td>
											<td><c:out value="${ogrenci.kacinciSinif}" /></td>
											<td><c:out value="${ogrenci.odaNo}" /></td>
											<td><a
												href="OgrenciController?ogrenciId=${ogrenci.ogrenciId}"
												class="btn btn-danger" type="button">Delete</a></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
					<div class="col-md-6"></div>
				</div>
			</div>
		</div>
	</div>

</body>
</html>