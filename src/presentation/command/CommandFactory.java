package presentation.command;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;

public class CommandFactory {
	private static CommandFactory factory = new CommandFactory();
	private final HashMap<String, String> commands = new HashMap<String, String>();

	private CommandFactory() {
		try {
			final ResourceBundle bundle = ResourceBundle.getBundle("properties/command");
			final Enumeration<String> en = bundle.getKeys();
			for (; en.hasMoreElements();) {
				final String key = en.nextElement();
				commands.put(key, bundle.getString(key));
			}
		} catch (final Exception e) {
			e.printStackTrace();
			throw new RuntimeException("");
		}
	}

	public static CommandFactory instance() {
		return factory;
	}

	public Command createCommand(final String action,
			final HttpServletRequest request) {
		Command command = null;
		try {
			command = (Command) Class.forName(commands.get(action)).newInstance();
			command.setRequest(request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return command;
	}
}
