package presentation.command;

import javax.servlet.http.HttpServletRequest;

import presentation.helper.RequestHelper;

public interface Command {
	
	public String execute(RequestHelper helper);
	
	public void setRequest(HttpServletRequest request);
	
}
