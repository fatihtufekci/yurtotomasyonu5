package presentation.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import model.Admin;
import presentation.factory.FacadeFactory;
import presentation.helper.RequestHelper;

public class LoginCommand implements Command{
	
	private HttpServletRequest request = null;
	
	public HttpServletRequest getRequest() {
		return request;
	}

	@Override
	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}
	
	@Override
	public synchronized String execute(RequestHelper helper) {
		String result = "";
		
		try {
			final Admin admin = new Admin();
			admin.setKullaniciAdi(getRequest().getParameter("kullaniciAdi"));
			admin.setParola(getRequest().getParameter("parola"));
			final Admin loginCheck = FacadeFactory.instance().loginCheck(admin);
			if(loginCheck != null) {
				getRequest().setAttribute("result", loginCheck);
				result="/index2.jsp";
			}else {
				result="/error.jsp";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
}
