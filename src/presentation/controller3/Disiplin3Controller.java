package presentation.controller3;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Disiplin;
import model.Ogrenci3;
import persistance.dao.JDBCOgrenci3;

@WebServlet("/admin/Disiplin3Controller")
public class Disiplin3Controller extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		if(request.getParameter("disiplinId")!=null) {
			int id = Integer.parseInt(request.getParameter("disiplinId"));
			JDBCOgrenci3 j = new JDBCOgrenci3();
			j.disiplinSil(id);
		}
		
		doPost(request, response);
		
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		JDBCOgrenci3 j = new JDBCOgrenci3();
		
		List<Ogrenci3> ogr= j.ogrenciListele();
		request.setAttribute("ogrenciler", ogr);
		
		List<Disiplin> disiplinListesi = j.disiplinListele();
		request.setAttribute("disiplin", disiplinListesi);
		
		
		RequestDispatcher rd = request.getRequestDispatcher("disiplin3.jsp");
		rd.forward(request, response);
		
	}

}
