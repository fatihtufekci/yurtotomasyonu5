package presentation.controller3;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import duzenle.ogrenci.DuzenleOgrenci;
import model.Oda;
import model.Ogrenci3;
import model.YurtFiyat;
import persistance.dao.JDBCOgrenci3;

@WebServlet("/admin/OgrenciListele3")
public class OgrenciListele3 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		if(request.getParameter("ogrenciId")!=null) {
			int id = Integer.parseInt(request.getParameter("ogrenciId"));
			JDBCOgrenci3 j = new JDBCOgrenci3();
			j.delete(id);
		}
		
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		JDBCOgrenci3 j = new JDBCOgrenci3();
		DuzenleOgrenci duzenle = new DuzenleOgrenci();
		
		List<Oda> oda = j.odaListele2();
		request.setAttribute("odalar", oda);
		
		List<Ogrenci3> ogrenci = j.ogrenciListele();
		request.setAttribute("ogrenciler", ogrenci);
		
		String hataliMesaj = "";
		String s = "";
		String kacinciSinif = "";
		kacinciSinif = request.getParameter("kacinciSinif");
		s = request.getParameter("oda");
		int buffer = 0;
		Ogrenci3 ogr = null;
		
		if(s!=null) {
			
			if(duzenle.textAlani(request.getParameter("ad")) && duzenle.textAlani(request.getParameter("soyad")) && 
					duzenle.sayiAlani(request.getParameter("tc")) && duzenle.sayiAlani(request.getParameter("tel")) && 
					duzenle.sayiAlani(request.getParameter("velitel")) && kacinciSinif!="") {
				ogr = new Ogrenci3();
				ogr.setAd(request.getParameter("ad"));
				ogr.setSoyad(request.getParameter("soyad"));
				ogr.setTc(Long.parseLong(request.getParameter("tc")));
				ogr.setTelefon(Long.parseLong(request.getParameter("tel")));
				ogr.setVeliTelefon(Long.parseLong(request.getParameter("velitel")));
				ogr.setKacinciSinif(Integer.parseInt(request.getParameter("kacinciSinif")));
				
				String odaId = request.getParameter("oda");
				Oda odaniz = j.getOdaId(Integer.parseInt(odaId));
				
				YurtFiyat yf = j.getYurtFiyat(odaniz.getOdaKapasite());
				
				ogr.setOda(odaniz);
				
				request.setAttribute("toplamTutar", yf.getToplamTutar());
				
				
				buffer = 1;
			}else {
				hataliMesaj = "Hatal� veri giri�i..!!";
			}
		}
		
		request.setAttribute("o", ogr);
		request.setAttribute("buffer", buffer);
		request.setAttribute("hataliMesaj", hataliMesaj);
		
		RequestDispatcher d = request.getRequestDispatcher("ogrenciListe3.jsp");
		try {
			d.forward(request, response);
		} catch (ServletException | IOException e) {
			e.printStackTrace();
		}
		
	}

}
