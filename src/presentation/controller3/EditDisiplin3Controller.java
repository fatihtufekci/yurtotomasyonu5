package presentation.controller3;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Disiplin;
import persistance.dao.JDBCOgrenci3;

@WebServlet("/admin/EditDisiplin3Controller")
public class EditDisiplin3Controller extends HttpServlet {
	   
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		JDBCOgrenci3 j = new JDBCOgrenci3();
		
		String idTemp = request.getParameter("id");
		int disiplinId = Integer.parseInt(idTemp);

		String sebep = request.getParameter("sebep");
		String cezasi = request.getParameter("cezasi");
		String ogrenciId = request.getParameter("ogrenci");
		
		if(!sebep.equals("") && !cezasi.equals("") && !ogrenciId.equals("")) {
			
			Disiplin d = new Disiplin();
			d.setSebep(sebep);
			d.setCezasi(cezasi);
			d.setOgrenciId(Integer.parseInt(ogrenciId));
			
			d.setDisiplinId(disiplinId);
			
			j.editDisiplin(d);
		}
		
		response.sendRedirect("Disiplin3Controller");
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
