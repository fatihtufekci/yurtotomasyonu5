package presentation.controller3;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import duzenle.ogrenci.DuzenleOgrenci;
import model.Oda;
import model.Ogrenci;
import model.Ogrenci3;
import persistance.dao.JDBCDAOImpl;
import persistance.dao.JDBCOgrenci3;

@WebServlet("/admin/EditOgrenci3Guncelle")
public class EditOgrenci3Guncelle extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		JDBCOgrenci3 j = new JDBCOgrenci3();
		DuzenleOgrenci duzenle = new DuzenleOgrenci();
		
		String ad = request.getParameter("ad");
		String soyad = request.getParameter("soyad");
		String tc = request.getParameter("tc");
		String tel = request.getParameter("tel");
		String velitel = request.getParameter("velitel");
		String kacinciSinif = request.getParameter("kacinciSinif");
		String odaNo = request.getParameter("oda");
		
		String idTemp = request.getParameter("id");
		int ogrenciId = Integer.parseInt(idTemp);
		
		String hataliMesaj = "";
		
		if(duzenle.textAlani(request.getParameter("ad")) && duzenle.textAlani(request.getParameter("soyad")) && 
				duzenle.sayiAlani(request.getParameter("tc")) && duzenle.sayiAlani(request.getParameter("tel")) && 
				duzenle.sayiAlani(request.getParameter("velitel")) && kacinciSinif!="" && odaNo != "") {
			
			System.out.println("T�fek�i");
			
			Ogrenci3 ogr = new Ogrenci3();
			ogr.setAd(ad);
			ogr.setSoyad(soyad);
			ogr.setTc(Long.parseLong(tc));
			ogr.setTelefon(Long.parseLong(tel));
			ogr.setVeliTelefon(Long.parseLong(velitel));
			ogr.setKacinciSinif(Integer.parseInt(kacinciSinif));
			
			Oda odaa = j.getOdaId(Integer.parseInt(odaNo));
			ogr.setOda(odaa);
			ogr.setOgrenciId(ogrenciId);
			
			j.editOgrenci(ogr);
			
			response.sendRedirect("OgrenciListele3");
		}else {
			hataliMesaj = "Hatal� veri giri�i..!!";
			request.setAttribute("hataliMesaj", hataliMesaj);
			
			request.getRequestDispatcher("EditOgrenci3?ogrenciId="+ogrenciId).forward(request, response);
		}
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
