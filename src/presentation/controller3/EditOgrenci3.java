package presentation.controller3;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Oda;
import model.Ogrenci;
import model.Ogrenci3;
import persistance.dao.JDBCDAOImpl;
import persistance.dao.JDBCOgrenci3;

@WebServlet("/admin/EditOgrenci3")
public class EditOgrenci3 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		JDBCOgrenci3 j = new JDBCOgrenci3();
		List<Oda> oda = j.odaListele2();
		request.setAttribute("odalar", oda);
		
		String idTemp = request.getParameter("ogrenciId");
		int id = Integer.parseInt(idTemp);
		request.setAttribute("o", j.getOgrenciId(id));

		RequestDispatcher rd = request.getRequestDispatcher("editOgrenci3.jsp");
		rd.forward(request, response);
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
