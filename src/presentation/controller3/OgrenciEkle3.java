package presentation.controller3;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Borc;
import model.Oda;
import model.Ogrenci3;
import model.YurtFiyat;
import persistance.dao.JDBCOgrenci3;


@WebServlet("/admin/OgrenciEkle3")
public class OgrenciEkle3 extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		doPost(request, response);
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		JDBCOgrenci3 j = new JDBCOgrenci3();
		String ad = request.getParameter("ad");
		String soyad = request.getParameter("soyad");
		String tc = request.getParameter("tc");
		String tel = request.getParameter("tel");
		String velitel = request.getParameter("velitel");
		String kacinciSinif = request.getParameter("kacinciSinif");
		String odaId = request.getParameter("oda");
		String borcVade = request.getParameter("borcVade");
		
		
		if(ad != "" && soyad != "" && tc != "" && tel != "" && velitel != "" && kacinciSinif != "" && odaId != "") {
			Ogrenci3 ogr = new Ogrenci3();
			ogr.setAd(ad);
			ogr.setSoyad(soyad);
			ogr.setTc(Long.parseLong(tc));
			ogr.setTelefon(Long.parseLong(tel));
			ogr.setVeliTelefon(Long.parseLong(velitel));
			ogr.setKacinciSinif(Integer.parseInt(kacinciSinif));
			Oda odaniz = j.getOdaId(Integer.parseInt(odaId));
			
			YurtFiyat yf = j.getYurtFiyat(odaniz.getOdaKapasite());
			
			ogr.setOda(odaniz);
			
			Borc borc = new Borc();
			int borcVadeInt = Integer.parseInt(borcVade);
			borc.setBorcVade(borcVadeInt);
			if(borcVadeInt==0) {
				borc.setBorcMiktari(0);
			}else if(borcVadeInt==4) {
				borc.setBorcMiktari(yf.getToplamTutar()/4);
			}else if(borcVadeInt==5) {
				borc.setBorcMiktari(yf.getToplamTutar()/5);
			}else if(borcVadeInt==6) {
				borc.setBorcMiktari(yf.getToplamTutar()/6);
			}else if(borcVadeInt==7) {
				borc.setBorcMiktari(yf.getToplamTutar()/7);
			}else if(borcVadeInt==8) {
				borc.setBorcMiktari(yf.getToplamTutar()/8);
			}
			
			String pattern = "yyyy-MM-dd";
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
			String date = simpleDateFormat.format(new Date());
			borc.setBorcTarihi(date);
			
			borc.setYurtFiyat(yf);
			
			int borcId = j.borcEkle(borc);
			
			borc.setBorcId(borcId);
			ogr.setBorc(borc);
			
			j.ogrenciEkle(ogr);
		}
		response.sendRedirect("OgrenciListele3");
	}

}
