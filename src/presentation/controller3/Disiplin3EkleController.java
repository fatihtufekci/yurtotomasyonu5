package presentation.controller3;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Disiplin;
import persistance.dao.JDBCOgrenci3;


@WebServlet("/admin/Disiplin3EkleController")
public class Disiplin3EkleController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		JDBCOgrenci3 j = new JDBCOgrenci3();
		
		String sebep = request.getParameter("sebep");
		String cezasi = request.getParameter("cezasi");
		String ogrenciId = request.getParameter("ogrenci");
		
		Disiplin disiplin = new Disiplin();
		disiplin.setSebep(sebep);
		disiplin.setCezasi(cezasi);
		disiplin.setOgrenciId(Integer.parseInt(ogrenciId));
		
		j.disiplinEkle(disiplin);
		
		response.sendRedirect("Disiplin3Controller");
	}

}
