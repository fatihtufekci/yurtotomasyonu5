package presentation.controller3;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Disiplin;
import model.Ogrenci3;
import persistance.dao.JDBCOgrenci3;

@WebServlet("/admin/EditListDisiplin3Controller")
public class EditListDisiplin3Controller extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		JDBCOgrenci3 j = new JDBCOgrenci3();
		
		String idTemp = request.getParameter("disiplinId");
		int id = Integer.parseInt(idTemp);
		request.setAttribute("getDisiplinById", j.getDisiplinId(id));
		
		List<Disiplin> liste = j.getDisiplinId(id);
		
		Ogrenci3 ogrenciListe = j.getOgrenciId(liste.get(0).getOgrenciId());
		
		List<Ogrenci3> listOgrenci = j.ogrenciListele();
		
		request.setAttribute("ogrenciler", listOgrenci);
		
		request.setAttribute("ogrenciListe", ogrenciListe);
		
		
		RequestDispatcher rd = request.getRequestDispatcher("editDisiplin3.jsp");
		rd.forward(request, response);
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
