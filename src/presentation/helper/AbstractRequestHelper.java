package presentation.helper;

import java.util.Enumeration;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public abstract class AbstractRequestHelper implements RequestHelper{
	
	private HttpServletRequest request;
	private HttpServletResponse response;
	private HashMap<String, String> requestProperties;
	public AbstractRequestHelper(HttpServletRequest request, HttpServletResponse response) {
		super();
		this.request = request;
		this.response = response;
		this.requestProperties = getProperties(request);
	}
	public HttpServletRequest getRequest() {
		return request;
	}
	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}
	public HttpServletResponse getResponse() {
		return response;
	}
	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}
	public HashMap<String, String> getRequestProperties() {
		return requestProperties;
	}
	
	public HashMap<String, String> getProperties(final HttpServletRequest request){
		final HashMap<String, String> properties = new HashMap<String, String>();
		try {
			
			final Enumeration<String> en = request.getParameterNames();
			for(; en.hasMoreElements() ;) {
				final String key = en.nextElement();
				final String value = request.getParameter(key);
				properties.put(key, value);
			}
		} catch (final Exception e) {
			e.printStackTrace();
		}
		
		return properties;
	}
	
	public void setRequestProperties(HashMap<String, String> requestProperties) {
		this.requestProperties = requestProperties;
	}
	
}
