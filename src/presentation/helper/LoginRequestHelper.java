package presentation.helper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import presentation.command.Command;
import presentation.command.CommandFactory;

public class LoginRequestHelper extends AbstractRequestHelper {

	public LoginRequestHelper(final HttpServletRequest request, final HttpServletResponse response) {
		super(request, response);
	}

	@Override
	public Command getCommand() {
		return CommandFactory.instance().createCommand(getRequestProperties().get("action"), getRequest());
	}
}
