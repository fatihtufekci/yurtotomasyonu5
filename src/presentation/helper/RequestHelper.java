package presentation.helper;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import presentation.command.Command;

public interface RequestHelper {
	
	HttpServletRequest getRequest();
	
	HttpServletResponse getResponse();
	
	Command getCommand();
	
	HashMap<String, String> getRequestProperties();
}
