package presentation.controller3_ogrenci;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Ogrenci3;
import persistance.dao.JDBCOgrenci3Impl;


@WebServlet("/ogrenci/BilgiListele")
public class BilgiListele extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		JDBCOgrenci3Impl j = new JDBCOgrenci3Impl();
		
		j.borcGuncelle();
		
		HttpSession session = request.getSession();
		Ogrenci3 obj = (Ogrenci3)session.getAttribute("user");
		
		Ogrenci3 liste = j.getOgrenci(obj.getOgrenciId());
		
		
		
		request.setAttribute("ogrenci", liste);
		
		RequestDispatcher rd = request.getRequestDispatcher("bilgiListele3.jsp");
		rd.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
