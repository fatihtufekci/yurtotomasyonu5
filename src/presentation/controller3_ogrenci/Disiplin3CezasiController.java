package presentation.controller3_ogrenci;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Disiplin;
import model.Ogrenci;
import model.Ogrenci3;
import persistance.dao.JDBCOgrenci3Impl;

@WebServlet("/ogrenci/Disiplin3CezasiController")
public class Disiplin3CezasiController extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		JDBCOgrenci3Impl j = new JDBCOgrenci3Impl();
		
		HttpSession session = request.getSession();
		Object obj = session.getAttribute("user");
		
		Ogrenci3 o = (Ogrenci3) obj;
		
		List<Disiplin> liste = j.disiplinGetir(o.getOgrenciId());
		
		try {
			Disiplin d = liste.get(0);
			request.setAttribute("disiplin", liste);
			request.setAttribute("buffer", 1);
		} catch (IndexOutOfBoundsException e) {
			request.setAttribute("message", "Disiplin Cezanız bulunmamaktadır.");
			request.setAttribute("buffer", 2);
		}
		
		RequestDispatcher rd = request.getRequestDispatcher("ogrenciDisiplin3.jsp");
		rd.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

}
