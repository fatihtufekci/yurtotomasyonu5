package presentation.controller3_ogrenci;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Admin;
import model.Ogrenci3;
import persistance.dao.JDBCOgrenci3;

@WebServlet("/ogrenci/OgrenciIndex")
public class OgrenciIndex extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String action = request.getParameter("action");
		
		if(action==null) {
			response.sendRedirect("login.jsp");
		}else if(action.equalsIgnoreCase("logout")) {
			HttpSession session = request.getSession();
			session.removeAttribute("user");
			response.sendRedirect("login.jsp");
		}else if(action.equalsIgnoreCase("login")) {
			response.sendRedirect("login.jsp");
		}
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.sendRedirect("../admin/login2.jsp");
	}

}
