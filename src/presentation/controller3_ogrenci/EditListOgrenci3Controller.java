package presentation.controller3_ogrenci;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import persistance.dao.JDBCOgrenci3Impl;

@WebServlet("/ogrenci/EditListOgrenci3Controller")
public class EditListOgrenci3Controller extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		JDBCOgrenci3Impl j = new JDBCOgrenci3Impl();
		
		String idTemp = request.getParameter("ogrenciId");
		int id = Integer.parseInt(idTemp);
		request.setAttribute("o", j.getOgrenci(id));
		
		RequestDispatcher rd = request.getRequestDispatcher("editOgrenci3.jsp");
		rd.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

}
