package presentation.controller3_ogrenci;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Duyuru;
import persistance.dao.JDBCOgrenci3Impl;

@WebServlet("/ogrenci/Ogrenci3ListController")
public class Ogrenci3ListController extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doPost(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		JDBCOgrenci3Impl j = new JDBCOgrenci3Impl();
		
		// HttpSession session = request.getSession();
		// Object obj = session.getAttribute("user");

		j.duyuruGuncelle();
		
		List<Duyuru> liste = j.duyuruListele();
		request.setAttribute("duyurular", liste);
		
		RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
		try {
			rd.forward(request, response);
		} catch (ServletException | IOException e) {
			response.sendRedirect("error.jsp");
		}
	}

}
