package presentation.controller3_ogrenci;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import duzenle.ogrenci.DuzenleOgrenci;
import model.Ogrenci;
import model.Ogrenci3;
import persistance.dao.JDBCOgrenci3Impl;

@WebServlet("/ogrenci/EditBilgiGuncelle3Controller")
public class EditBilgiGuncelle3Controller extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		JDBCOgrenci3Impl j = new JDBCOgrenci3Impl();
		DuzenleOgrenci duzenle = new DuzenleOgrenci();
		
		String ad = request.getParameter("ad");
		String soyad = request.getParameter("soyad");
		String tc = request.getParameter("tc");
		String tel = request.getParameter("tel");
		
		String idTemp = request.getParameter("id");
		int ogrenciId = Integer.parseInt(idTemp);
		
		String hataliMesaj = "";
		
		if(duzenle.textAlani(request.getParameter("ad")) && duzenle.textAlani(request.getParameter("soyad")) && 
				duzenle.sayiAlani(request.getParameter("tc")) && duzenle.sayiAlani(request.getParameter("tel"))) {
			
			Ogrenci3 ogr = new Ogrenci3();
			ogr.setAd(ad);
			ogr.setSoyad(soyad);
			ogr.setTc(Long.parseLong(tc));
			ogr.setTelefon(Long.parseLong(tel));
			ogr.setOgrenciId(ogrenciId);
			
			j.ogrenciGuncelle(ogr);
			
			response.sendRedirect("BilgiListele");
		}else {
			hataliMesaj = "Hatal� veri giri�i..!!";
			request.setAttribute("hataliMesaj", hataliMesaj);
			
			request.getRequestDispatcher("EditListOgrenci3Controller?ogrenciId="+ogrenciId).forward(request, response);
		}
		
	}

}
