package presentation.controller_yeni;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import duzenle.ogrenci.OgrenciExcelCikar;
import model.Ogrenci3;
import persistance.dao.JDBCOgrenci3;

@WebServlet("/admin/OgrenciExcel")
public class OgrenciExcel extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//OgrenciExcelCikar ogr = new OgrenciExcelCikar();
		//ogr.writeExcel();
		
		JDBCOgrenci3 j = new JDBCOgrenci3();
		
		List<Ogrenci3> liste = j.ogrenciListele();
		
		response.setContentType("application/ms-excel");
		response.setHeader("Content-Disposition", "inline; filename=ogrenciList.xls");

		request.setAttribute("ogrenciler", liste);
		
		request.getRequestDispatcher("excelListe.jsp").forward(request, response);
		//response.sendRedirect("OgrenciListele3");
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
