package presentation.controller_yeni;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import persistance.dao.JDBCYeni;


@WebServlet("/admin/OdemeYapsanaController")
public class OdemeYapsanaController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		JDBCYeni j = new JDBCYeni();
		
		String idTemp = request.getParameter("giderId");
		int giderId = Integer.parseInt(idTemp);
		
		boolean check = j.odemeYap(giderId);
		
		String a = "";
		if(check) {
			a = "�deme Ba�ar�l�";
		}else {
			a = "�deme Ba�ar�s�z. Bakiye yetersiz..";
		}
		request.setAttribute("odemeMesaj", a);
		
		request.getRequestDispatcher("GiderListController").forward(request, response);
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
