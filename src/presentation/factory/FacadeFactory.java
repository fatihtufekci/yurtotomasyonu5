package presentation.factory;

import java.util.ResourceBundle;

import business.facade.BusinessFacade;
import model.Admin;

public class FacadeFactory {

	public static FacadeFactory factory;

	private BusinessFacade businessFacade = null;

	public BusinessFacade getBusinessFacade() {
		return businessFacade;
	}

	public void setBusinessFacade(final BusinessFacade businessFacade) {
		this.businessFacade = businessFacade;
	}

	private FacadeFactory() {
		initFactory();
	}

	public static FacadeFactory instance() {
		if (factory == null) {
			factory = new FacadeFactory();
		}
		return factory;
	}

	private void initFactory() {
		try {
			final String facade = ResourceBundle.getBundle("properties/presentation").getString("businessfacade");
			businessFacade = (BusinessFacade) Class.forName(facade).newInstance();
		} catch (final Exception e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
	}

	public Admin loginCheck(final Admin search) {
		return businessFacade.loginCheck(search);
	}
}
