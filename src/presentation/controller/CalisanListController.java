package presentation.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Calisan;
import model.Gorev;
import persistance.dao.JDBCDAOImpl;
import persistance.dao.JDBCDAOImpl2;

@WebServlet("/admin/CalisanListController")
public class CalisanListController extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		if(request.getParameter("calisanId")!=null) {
			int id = Integer.parseInt(request.getParameter("calisanId"));
			JDBCDAOImpl2 j2 = new JDBCDAOImpl2();
			j2.calisanSil(id);
		}
		doPost(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		JDBCDAOImpl2 j2 = new JDBCDAOImpl2();
		
		List<Gorev> gorevler = j2.gorevListele();
		
		request.setAttribute("gorevler", gorevler);
		
		List<Calisan> calisanlar = j2.calisanListele();
		
		request.setAttribute("calisanlar", calisanlar);
		
		RequestDispatcher rd = request.getRequestDispatcher("calisan.jsp");
		rd.forward(request, response);
	}

}
