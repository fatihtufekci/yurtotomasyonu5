package presentation.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Calisan;
import model.Disiplin;
import model.Gorev;
import model.Ogrenci;
import persistance.dao.JDBCDAOImpl;
import persistance.dao.JDBCDAOImpl2;


@WebServlet("/admin/EditListCalisanController")
public class EditListCalisanController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		JDBCDAOImpl2 j = new JDBCDAOImpl2();
		
		String idTemp = request.getParameter("calisanId");
		int id = Integer.parseInt(idTemp);
		request.setAttribute("getCalisanById", j.getCalisanId(id));
		
		List<Gorev> gorevler = j.gorevListele();
		request.setAttribute("gorevler", gorevler);
		
		
		RequestDispatcher rd = request.getRequestDispatcher("editCalisan.jsp");
		rd.forward(request, response);
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
