package presentation.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Duyuru;
import persistance.dao.JDBCDAOImpl;

@WebServlet("/admin/EditDuyuruController")
public class EditDuyuruController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		JDBCDAOImpl j = new JDBCDAOImpl();
		
		String idTemp = request.getParameter("id");
		int duyuruId = Integer.parseInt(idTemp);

		String konu = request.getParameter("konu");
		String icerik = request.getParameter("icerik");
		String tarih = request.getParameter("tarih");
		
		
		if(!konu.equals("") && !icerik.equals("") && !tarih.equals("")) {
			
			Duyuru d = new Duyuru();
			d.setDuyuruKonu(konu);
			d.setDuyuruIcerik(icerik);
			d.setDuyuruTarih(tarih);
			d.setDuyuruId(duyuruId);
			
			j.editDuyuru(d);
		}
		
		response.sendRedirect("DuyuruListeleController");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

}
