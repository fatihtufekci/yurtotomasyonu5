package presentation.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import presentation.command.Command;
import presentation.helper.LoginRequestHelper;
import presentation.helper.RequestHelper;

@WebServlet("/LoginController2")
public class LoginController2 extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	public void handle(final HttpServletRequest request, final HttpServletResponse response){
		
		String nextPage = "";
		
		try {
			final RequestHelper helper = new LoginRequestHelper(request, response);
			final Command command = helper.getCommand();
			nextPage = command.execute(helper);
			//response.sendRedirect(request.getContextPath() + nextPage);
			dispatch(request, response, nextPage);
		} catch (Exception e) {
			e.printStackTrace();
			try {
				dispatch(request, response, "/error.jsp");
			} catch (final Exception e2) {
				e2.printStackTrace();
			}
		}
		
	}
	
	private void dispatch(final HttpServletRequest request, final HttpServletResponse response, final String page) throws ServletException, IOException {
		final RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
		dispatcher.forward(request, response);
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String action = request.getParameter("action");
		
		if(action == null) {
			request.getRequestDispatcher("login2.jsp").forward(request, response);
		}else if(action.equalsIgnoreCase("login")) {
			request.getRequestDispatcher("login2.jsp").forward(request, response);
		}else if(action.equalsIgnoreCase("logout")) {
			HttpSession session = request.getSession();
			session.removeAttribute("user");
			request.getRequestDispatcher("login2.jsp").forward(request, response);
		}
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		handle(request, response);
	}

}
