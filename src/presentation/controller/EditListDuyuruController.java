package presentation.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Duyuru;
import persistance.dao.JDBCDAOImpl;


@WebServlet("/admin/EditListDuyuruController")
public class EditListDuyuruController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		JDBCDAOImpl j = new JDBCDAOImpl();
		
		String idTemp = request.getParameter("duyuruId");
		int id = Integer.parseInt(idTemp);
		request.setAttribute("getDuyuruById", j.getDuyuruId(id));
		//request.setAttribute("o", j.getById(id).get(0));
		
		List<Duyuru> liste = j.getDuyuruId(id);
		
		RequestDispatcher rd = request.getRequestDispatcher("editDuyuru.jsp");
		rd.forward(request, response);
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		doGet(request, response);
	}

}
