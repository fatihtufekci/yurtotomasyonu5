package presentation.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import duzenle.ogrenci.DuzenleOgrenci;
import model.Calisan;
import model.Disiplin;
import model.Gorev;
import persistance.dao.JDBCDAOImpl;
import persistance.dao.JDBCDAOImpl2;

@WebServlet("/admin/EditCalisanController")
public class EditCalisanController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		JDBCDAOImpl2 j = new JDBCDAOImpl2();
		DuzenleOgrenci duzenle = new DuzenleOgrenci();
		
		String idTemp = request.getParameter("id");
		int calisanId = Integer.parseInt(idTemp);

		String ad = request.getParameter("ad");
		String soyad = request.getParameter("soyad");
		String tc = request.getParameter("tc");
		String telefon = request.getParameter("tel");
		String maas = request.getParameter("maas");
		String gorev = request.getParameter("gorev");
		
		String hataliMesaj = "";
		
		if(duzenle.textAlani(request.getParameter("ad")) && duzenle.textAlani(request.getParameter("soyad")) && 
				duzenle.sayiAlani(request.getParameter("tc")) && duzenle.sayiAlani(request.getParameter("tel")) && 
				maas != "" && gorev != "") {
			
			Calisan c = new Calisan();
			c.setCalisanId(calisanId);
			c.setAd(ad);
			c.setSoyad(soyad);
			c.setTc(Long.valueOf(tc));
			c.setTelefon(Long.parseLong(telefon));
			c.setMaas(Integer.parseInt(maas));
			
			List<Gorev> gorevii = j.getGorevId(Integer.parseInt(gorev));
			c.setGorev(gorevii.get(0));
			
			j.editCalisan(c);
			
			response.sendRedirect("CalisanListController");
		}else {
			hataliMesaj = "Hatal� veri giri�i..!!";
			request.setAttribute("hataliMesaj", hataliMesaj);
			
			request.getRequestDispatcher("EditListCalisanController?calisanId="+calisanId).forward(request, response);
		}
		
		
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
