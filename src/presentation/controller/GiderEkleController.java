package presentation.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Gider;
import persistance.dao.JDBCDAOImpl2;

@WebServlet("/admin/GiderEkleController")
public class GiderEkleController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		JDBCDAOImpl2 j = new JDBCDAOImpl2();
		String elektrik = request.getParameter("elektrik");
		String su = request.getParameter("su");
		String dogalgaz = request.getParameter("dogalgaz");
		String yemekGideri = request.getParameter("yemekGideri");
		String giderTarihi = request.getParameter("giderTarihi");
		
		
		if(elektrik != "" && su != "" && dogalgaz != "" && yemekGideri != "" && giderTarihi != "") {
			Gider g = new Gider();
			
			g.setElektrik(Integer.parseInt(elektrik));
			g.setSu(Integer.parseInt(su));
			g.setDogalgaz(Integer.parseInt(dogalgaz));
			g.setGiderTarihi(giderTarihi);
			g.setYemekGideri(Integer.parseInt(yemekGideri));
			
			j.giderEkle(g);

		}
		
		response.sendRedirect("GiderListController");
		
	}

}
