package presentation.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Duyuru;
import persistance.dao.JDBCDAOImpl;
import persistance.dao.JDBCOgrenci3Impl;

@WebServlet("/admin/DuyuruListeleController")
public class DuyuruListeleController extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		if(request.getParameter("duyuruId")!=null) {
			int id = Integer.parseInt(request.getParameter("duyuruId"));
			JDBCDAOImpl j = new JDBCDAOImpl();
			j.duyuruSil(id);
		}
		
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		JDBCOgrenci3Impl j = new JDBCOgrenci3Impl();
		
		j.duyuruGuncelle();
		
		List<Duyuru> duyuru = j.duyuruListele();
		
		request.setAttribute("duyurular", duyuru);
		
		RequestDispatcher rd = request.getRequestDispatcher("duyuru.jsp");
		rd.forward(request, response);
	}

}
