package presentation.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import presentation.command.Command;
import presentation.helper.LoginRequestHelper;
import presentation.helper.RequestHelper;

@WebServlet("/LoginController")
public class LoginController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	public void handle(final HttpServletRequest request, final HttpServletResponse response){
		
		String nextPage = "";
		
		try {
			final RequestHelper helper = new LoginRequestHelper(request, response);
			final Command command = helper.getCommand();
			nextPage = command.execute(helper);
			//response.sendRedirect(request.getContextPath() + nextPage);
			dispatch(request, response, nextPage);
		} catch (Exception e) {
			e.printStackTrace();
			try {
				dispatch(request, response, "/error.jsp");
			} catch (final Exception e2) {
				e2.printStackTrace();
			}
		}
		
//		String kullaniciAdi = request.getParameter("kullaniciAdi");
//		String parola = request.getParameter("parola");
//		System.out.println(kullaniciAdi);
//		System.out.println(parola);
//		Kisi k = new Kisi();
//		k.setkAdi(kullaniciAdi);
//		k.setParola(parola);
//		
//		request.setAttribute("kisi", k);
//		
//		RequestDispatcher d = request.getRequestDispatcher("member.jsp");
//		try {
//			d.forward(request, response);
//		} catch (ServletException | IOException e) {
//			e.printStackTrace();
//		}
	}
	
	private void dispatch(final HttpServletRequest request, final HttpServletResponse response, final String page) throws ServletException, IOException {
		final RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
		dispatcher.forward(request, response);
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		handle(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		handle(request, response);
	}

}
