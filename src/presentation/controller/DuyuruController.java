package presentation.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Duyuru;
import model.Ogrenci;
import persistance.dao.JDBCDAOImpl;

@WebServlet("/admin/DuyuruController")
public class DuyuruController extends HttpServlet {
	private static final long serialVersionUID = 1L;
 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		JDBCDAOImpl j = new JDBCDAOImpl();
		
		
		String duyuruKonu = request.getParameter("duyuruKonu");
		String duyuruIcerik = request.getParameter("duyuruIcerik");
		String duyuruTarih = request.getParameter("duyuruTarih");
		
		Duyuru duyuru = new Duyuru();
		duyuru.setDuyuruKonu(duyuruKonu);
		duyuru.setDuyuruIcerik(duyuruIcerik);
		duyuru.setDuyuruTarih(duyuruTarih);
		
		j.duyuruEkle(duyuru);
		
		//RequestDispatcher d = request.getRequestDispatcher("duyuru.jsp");
		//d.forward(request, response);
		
		response.sendRedirect("DuyuruListeleController");
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
		
	}

}
