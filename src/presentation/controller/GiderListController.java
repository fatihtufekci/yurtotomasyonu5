package presentation.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Gider;
import persistance.dao.JDBCDAOImpl2;

@WebServlet("/admin/GiderListController")
public class GiderListController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		JDBCDAOImpl2 j = new JDBCDAOImpl2();
		
		List<Gider> giderler = j.giderListele();
		
		request.setAttribute("giderler", giderler);
		
		String a = request.getParameter("odemeMesaj");
		request.setAttribute("messages", a);
		
		RequestDispatcher rd = request.getRequestDispatcher("giderler.jsp");
		rd.forward(request, response);
	}

}
