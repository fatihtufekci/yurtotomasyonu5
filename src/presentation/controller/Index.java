package presentation.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import duzenle.ogrenci.SQLInjection;
import model.Admin;
import model.Ogrenci;
import model.Ogrenci3;
import persistance.dao.JDBCDAOImpl2;
import persistance.dao.JDBCOgrenci3;

@WebServlet("/admin/Index")
public class Index extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			
		String action = request.getParameter("action");
		
		if(action==null) {
			response.sendRedirect("login2.jsp");
		}else if(action.equalsIgnoreCase("logout")) {
			HttpSession session = request.getSession();
			session.removeAttribute("user");
			response.sendRedirect("login2.jsp");
		}else if(action.equalsIgnoreCase("login")) {
			response.sendRedirect("login2.jsp");
			//request.getRequestDispatcher("login2.jsp").forward(request, response);
		}
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
//		String username = request.getParameter("kullaniciAdi");
//		String password = request.getParameter("parola");
//		
//		if(username.equalsIgnoreCase("index") && password.equalsIgnoreCase("index")) {
//			HttpSession session = request.getSession();
//			session.setAttribute("user", username);
//			request.getRequestDispatcher("index2.jsp").forward(request, response);
//		}else {
//			request.setAttribute("messages", "HATALI G�R��");
//			//request.getRequestDispatcher("login2.jsp").forward(request, response);
//			response.sendRedirect("login2.jsp");
//		}
		
		JDBCOgrenci3 j = new JDBCOgrenci3();
		String username = request.getParameter("kullaniciAdi");
		String password = request.getParameter("parola");
		
		boolean sqlInj = new SQLInjection().duzgunVeri(username, password);
		if(sqlInj) {
			Object a = j.login(username, password);
			if(a instanceof Admin) {
				HttpSession session = request.getSession();
				session.setAttribute("user", (Admin)a);
				response.sendRedirect("index2.jsp");
			}else if(a instanceof Ogrenci3){
				HttpSession session = request.getSession();
				session.setAttribute("user", (Ogrenci3)a);
				response.sendRedirect("../ogrenci/Ogrenci3ListController");
			}else {
				request.setAttribute("messages", "HATALI G�R��");
				request.getRequestDispatcher("login2.jsp").forward(request, response);
			}
		}else {
			request.setAttribute("messages", "HATALI G�R��");
			request.getRequestDispatcher("login2.jsp").forward(request, response);
		}
		
		
	}

}
