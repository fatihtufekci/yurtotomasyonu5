package presentation.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import duzenle.ogrenci.DuzenleOgrenci;
import model.Calisan;
import model.Gorev;
import model.Ogrenci;
import persistance.dao.JDBCDAOImpl;
import persistance.dao.JDBCDAOImpl2;

@WebServlet("/admin/CalisanEkleController")
public class CalisanEkleController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doPost(request, response);
		
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			
		JDBCDAOImpl2 j = new JDBCDAOImpl2();
		DuzenleOgrenci duzenle = new DuzenleOgrenci();
		
		String ad = request.getParameter("ad");
		String soyad = request.getParameter("soyad");
		String tc = request.getParameter("tc");
		String tel = request.getParameter("tel");
		String maas = request.getParameter("maas");
		String gorev = request.getParameter("gorev");
		
		int gorevId = Integer.parseInt(gorev);
		
		if(duzenle.textAlani(request.getParameter("ad")) && duzenle.textAlani(request.getParameter("soyad")) && 
				duzenle.sayiAlani(request.getParameter("tc")) && duzenle.sayiAlani(request.getParameter("tel")) && 
				maas != "" && gorev != "") {
			
			Calisan c = new Calisan();
			c.setAd(ad);
			c.setSoyad(soyad);
			c.setTc(Long.parseLong(tc));
			c.setTelefon(Long.parseLong(tel));
			c.setMaas(Integer.parseInt(maas));
			
			List<Gorev> gorevList = j.getGorevId(gorevId);
			
			c.setGorev(gorevList.get(0));
			
			j.calisanEkle(c);

		}
		
		response.sendRedirect("CalisanListController");
	}

}
