package persistance.dao;

import java.util.List;

import model.Admin;
import model.Oda;
import model.Ogrenci;

public interface DAO {

	Admin check(Admin admin);
	
	List<Oda> odaListele();
	
	List<Ogrenci> ogrenciListele();
	
	boolean ogrenciEkle(Ogrenci ogrenci);
	
}
