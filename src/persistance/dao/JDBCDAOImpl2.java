package persistance.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Admin;
import model.Calisan;
import model.Gider;
import model.Gorev;
import model.Ogrenci;
import util.JDBCConnection;

public class JDBCDAOImpl2 {

	public void calisanEkle(Calisan calisan) {

		Connection conn = JDBCConnection.getConnection();
		PreparedStatement psmt = null;
		String query = "insert into calisan(TC, Ad, Soyad, Telefon, gorevId, Maas) values(?,?,?,?,?,?)";

		try {
			psmt = conn.prepareStatement(query);

			psmt.setString(1, String.valueOf(calisan.getTc()));
			psmt.setString(2, calisan.getAd());
			psmt.setString(3, calisan.getSoyad());
			psmt.setString(4, String.valueOf(calisan.getTelefon()));

			psmt.setInt(5, calisan.getGorev().getGorevId());
			psmt.setInt(6, calisan.getMaas());

			psmt.execute();

			psmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void giderEkle(Gider gider) {
		Connection conn = JDBCConnection.getConnection();
		PreparedStatement psmt = null;
		ResultSet rs = null;
		
		String query = "select Maas from calisan";
		String query2 = "insert into giderler(elektrik, su, dogalgaz, maaslar, yemekGideri, giderTarihi, odemeYapildi) values(?,?,?,?,?,?,?)";
		
		
		try {
			psmt = conn.prepareStatement(query);
			rs = psmt.executeQuery();
			int maas = 0;
			while(rs.next()) {
				maas += rs.getInt(1);
			}
			
			psmt = conn.prepareStatement(query2);
			psmt.setInt(1, gider.getElektrik());
			psmt.setInt(2, gider.getSu());
			psmt.setInt(3, gider.getDogalgaz());
			psmt.setInt(4, maas);
			psmt.setInt(5, gider.getYemekGideri());
			psmt.setString(6, gider.getGiderTarihi());
			psmt.setInt(7, 0);
			
			psmt.execute();

			psmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public List<Gorev> gorevListele(){
		List<Gorev> liste = new ArrayList<>();

		Connection conn = JDBCConnection.getConnection();
		PreparedStatement psmt = null;
		ResultSet rs = null;
		
		String query = "select * from gorev";
		
		try {
			psmt = conn.prepareStatement(query);
			rs = psmt.executeQuery();
			Gorev g = null;
			while(rs.next()) {
				g = new Gorev();
				g.setGorevId(rs.getInt(1));
				g.setGorev(rs.getString(2));
				
				liste.add(g);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return liste;
	}
	
	public List<Gider> giderListele(){

		List<Gider> liste = new ArrayList<>();
		Connection conn = JDBCConnection.getConnection();
		PreparedStatement psmt = null;
		ResultSet rs = null;

		String query = "select * from giderler where odemeYapildi=0";
		String query2 = "select Maas from calisan";
		
		try {
			psmt = conn.prepareStatement(query);
			rs = psmt.executeQuery();
			
			
			Gider o1 = new Gider();
			
			while (rs.next()) {
				Gider o = (Gider) o1.clone();
				o.setGiderId(rs.getInt(1));
				o.setElektrik(rs.getInt(2));
				o.setSu(rs.getInt(3));
				o.setDogalgaz(rs.getInt(4));
				o.setMaaslar(rs.getInt(5));
				o.setYemekGideri(rs.getInt(6));
				o.setGiderTarihi(rs.getString(7));
				
				liste.add(o);
			}

			psmt.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return liste;
	}
	
	public List<Calisan> calisanListele() {

		List<Calisan> liste = new ArrayList<>();

		Connection conn = JDBCConnection.getConnection();
		PreparedStatement psmt = null;
		ResultSet rs, rs2 = null;

		String query = "select * from calisan";
		String query2 = "select * from gorev where gorevId=?";
		
		try {
			psmt = conn.prepareStatement(query);
			rs = psmt.executeQuery();
			
			Gorev g = null;
			Calisan o1 = new Calisan();
			
			while (rs.next()) {
				Calisan o = (Calisan) o1.clone();
				o.setCalisanId(rs.getInt(1));
				o.setTc(rs.getLong(2));
				o.setAd(rs.getString(3));
				o.setSoyad(rs.getString(4));
				o.setTelefon(rs.getLong(5));
				o.setMaas(rs.getInt(7));
				
				psmt = conn.prepareStatement(query2);
				psmt.setInt(1, rs.getInt(6));
				rs2 = psmt.executeQuery();
				rs2.next();
				g = new Gorev();
				g.setGorevId(rs2.getInt(1));
				g.setGorev(rs2.getString(2));
				o.setGorev(g);

				liste.add(o);
			}

			psmt.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return liste;
	}
	
	public List<Gorev> getGorevId(int id){
		List<Gorev> list = new ArrayList<>();
		String sql = "select * from gorev where gorevId=" + id;
		
		Connection conn = JDBCConnection.getConnection();
		PreparedStatement psmt = null;
		ResultSet rs = null;

		try {
			psmt = conn.prepareStatement(sql);
			rs = psmt.executeQuery();

			rs.next();
			
			Gorev g = new Gorev();
			g.setGorevId(rs.getInt(1));
			g.setGorev(rs.getString(2));
			
			list.add(g);
			
			psmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return list;
	}
	
	public List<Calisan> getCalisanId(int id) {

		List<Calisan> list = new ArrayList<>();
		String sql = "select * from calisan where calisanId=" + id;
		
		String sql2 = "select * from gorev where gorevId=?";
		
		Connection conn = JDBCConnection.getConnection();
		PreparedStatement psmt = null;
		ResultSet rs, rs2 = null;

		try {
			psmt = conn.prepareStatement(sql);
			rs = psmt.executeQuery();

			rs.next();
			Calisan o = new Calisan();
			o.setTc(rs.getLong(2));
			o.setCalisanId(id);
			o.setAd(rs.getString(3));
			o.setSoyad(rs.getString(4));
			o.setTelefon(rs.getLong(5));
			
			Gorev g = new Gorev();
			psmt = conn.prepareStatement(sql2);
			psmt.setInt(1, rs.getInt(6));
			rs2 = psmt.executeQuery();
			rs2.next();
			g.setGorevId(rs2.getInt(1));
			g.setGorev(rs2.getString(2));
			
			o.setGorev(g);
			o.setMaas(rs.getInt(7));
			
			list.add(o);
			
			psmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return list;
	}

	public void editCalisan(Calisan o) {
		String query = "update calisan set TC=?, Ad=?, Soyad=?, Telefon=?, gorevId=?, Maas=?" + " where calisanId=?";

		Connection conn = JDBCConnection.getConnection();
		PreparedStatement psmt = null;

		try {
			psmt = conn.prepareStatement(query);
			psmt.setString(1, String.valueOf(o.getTc()));
			psmt.setString(2, o.getAd());
			psmt.setString(3, o.getSoyad());
			psmt.setString(4, String.valueOf(o.getTelefon()));
			psmt.setInt(5, o.getGorev().getGorevId());
			psmt.setInt(6, o.getMaas());
			
			psmt.setInt(7, o.getCalisanId());

			psmt.executeUpdate();

			psmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void calisanSil(int id) {
		String query = "delete from calisan where calisanId=?";

		Connection conn = JDBCConnection.getConnection();
		PreparedStatement psmt = null;

		try {
			psmt = conn.prepareStatement(query);
			psmt.setInt(1, id);
			psmt.execute();

			psmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public Admin adminLogin(String username, String password) {

		Admin a = new Admin();
		Connection conn = JDBCConnection.getConnection();
		PreparedStatement psmt = null;
		ResultSet rs = null;

		String query = "select * from admin where kullaniciAdi=? and parola=?";
		
		try {
			psmt = conn.prepareStatement(query);
			psmt.setString(1, username);
			psmt.setString(2, password);
			
			rs = psmt.executeQuery();
			
			if(rs.next()) {
				a.setAd(rs.getString(2));
				a.setSoyad(rs.getString(3));
				a.setGorevi(rs.getString(4));
				a.setKullaniciAdi(rs.getString(5));
				a.setParola(rs.getString(6));
				
				psmt.close();
				conn.close();
			}else {
				return null;
			}
			
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		return a;
	}
	
	public Ogrenci ogrenciLogin(String username, String password) {

		Ogrenci a = new Ogrenci();
		Connection conn = JDBCConnection.getConnection();
		PreparedStatement psmt = null;
		ResultSet rs = null;

		String query = "select * from ogrenci where kullaniciAdi=? and parola=?";
		
		try {
			psmt = conn.prepareStatement(query);
			psmt.setString(1, username);
			psmt.setString(2, password);
			
			rs = psmt.executeQuery();
			
			if(rs.next()) {
				a.setOgrenciId(rs.getInt(1));
				a.setAd(rs.getString(2));
				a.setSoyad(rs.getString(3));
				a.setTc(rs.getLong(4));
				a.setTelefon(rs.getLong(5));
				a.setVeliTelefon(rs.getLong(6));
				a.setKacinciSinif(rs.getInt(7));
				a.setOdaNo(rs.getInt(8));
				// a.setBorcId(rs.getInt(9));
				a.setBorcMiktari(rs.getInt(10));
				a.setKullaniciAdi(rs.getString(11));
				a.setParola(rs.getString(12));
				
				psmt.close();
				conn.close();
			}else {
				return null;
			}
			
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		return a;
	}
	
	public Object login(String username, String password) {

		Object a = null;
		Connection conn = JDBCConnection.getConnection();
		PreparedStatement psmt = null;
		ResultSet rs, rs2 = null;

		String query = "select * from ogrenci where kullaniciAdi=? and parola=?";
		
		String query2 = "select * from admin where kullaniciAdi=? and parola=? and gorevi='M�d�r'";
		
		try {
			psmt = conn.prepareStatement(query);
			psmt.setString(1, username);
			psmt.setString(2, password);
			
			rs = psmt.executeQuery();
			
			if(rs.next()) {
				a = new Ogrenci();
				((Ogrenci) a).setOgrenciId(rs.getInt(1));
				((Ogrenci) a).setAd(rs.getString(2));
				((Ogrenci) a).setSoyad(rs.getString(3));
				((Ogrenci) a).setTc(rs.getLong(4));
				((Ogrenci) a).setTelefon(rs.getLong(5));
				((Ogrenci) a).setVeliTelefon(rs.getLong(6));
				((Ogrenci) a).setKacinciSinif(rs.getInt(7));
				((Ogrenci) a).setOdaNo(rs.getInt(8));
				// a.setBorcId(rs.getInt(9));
				((Ogrenci) a).setBorcMiktari(rs.getInt(10));
				((Ogrenci) a).setKullaniciAdi(rs.getString(11));
				((Ogrenci) a).setParola(rs.getString(12));
				
			}else {
				psmt = conn.prepareStatement(query2);
				psmt.setString(1, username);
				psmt.setString(2, password);
				rs2 = psmt.executeQuery();
				if(rs2.next()) {
					a = new Admin();
					((Admin) a).setAdminId(rs2.getInt(1));
					((Admin) a).setAd(rs2.getString(2));
					((Admin) a).setSoyad(rs2.getString(3));
					((Admin) a).setGorevi(rs2.getString(4));
					((Admin) a).setKullaniciAdi(rs2.getString(5));
					((Admin) a).setParola(rs2.getString(6));
				}
			}
			
			psmt.close();
			conn.close();
			
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		return a;
	}
	
}
