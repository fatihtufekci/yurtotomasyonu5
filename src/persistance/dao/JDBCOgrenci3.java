package persistance.dao;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import model.Admin;
import model.Borc;
import model.Disiplin;
import model.Oda;
import model.Ogrenci3;
import model.YurtFiyat;
import util.JDBCConnection;

public class JDBCOgrenci3 {
	
	public List<Ogrenci3> ogrenciListele() {

		List<Ogrenci3> liste = new ArrayList<>();

		Connection conn = JDBCConnection.getConnection();
		PreparedStatement psmt = null;
		ResultSet rs, rs2, rs3, rs4 = null;

		String query = "select * from ogrenci3";
		String query2 = "select * from oda where odaNo=?";
		String query3 = "select * from yurtfiyat where yurtfiyatId=?";
		String query4 = "select * from borc2 where borcId=?";

		try {
			psmt = conn.prepareStatement(query);
			rs = psmt.executeQuery();
			Ogrenci3 o1 = new Ogrenci3();
			Oda oda = null;
			Borc borc = null;
			YurtFiyat yf = null;
			while (rs.next()) {
				Ogrenci3 o = (Ogrenci3) o1.clone();
				o.setOgrenciId(rs.getInt(1));
				o.setAd(rs.getString(2));
				o.setSoyad(rs.getString(3));
				o.setTc(rs.getLong(4));
				o.setTelefon(rs.getLong(5));
				o.setVeliTelefon(rs.getLong(6));
				o.setKacinciSinif(rs.getInt(7));
				o.setKullaniciAdi(rs.getString(10));
				o.setKartId(rs.getString(12));
				
				psmt = conn.prepareStatement(query2);
				psmt.setInt(1, rs.getInt(8));
				rs2 = psmt.executeQuery();
				rs2.next();
				oda = new Oda();
				oda.setOdaId(rs2.getInt(1));
				oda.setOdaNo(rs2.getInt(2));
				oda.setOdaKapasite(rs2.getInt(3));
				o.setOda(oda);
				
				
				psmt = conn.prepareStatement(query4);
				psmt.setInt(1, rs.getInt(9));
				rs4 = psmt.executeQuery();
				rs4.next();
				borc = new Borc();
				borc.setBorcId(rs.getInt(9));
				borc.setBorcMiktari(rs4.getInt(2));
				borc.setBorcVade(rs4.getInt(3));
				borc.setBorcTarihi(rs4.getString(4));
				
				psmt = conn.prepareStatement(query3);
				psmt.setInt(1, rs4.getInt(5));
				rs3 = psmt.executeQuery();
				rs3.next();
				yf = new YurtFiyat();
				yf.setYurtFiyatId(rs3.getInt(1));
				yf.setToplamTutar(rs3.getInt(2));
				yf.setOdaKapasitesi(oda.getOdaKapasite());
				borc.setYurtFiyat(yf);
				
				o.setBorc(borc);
				
				liste.add(o);
			}
			
			psmt.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return liste;
	}
	
	public int borcEkle(Borc borc) {
		Connection conn = JDBCConnection.getConnection();
		PreparedStatement psmt = null;
		String query = "insert into borc2(borcMiktari, borcVade, borcTarihi, yurtfiyatId) values(?,?,?,?)";
		
		ResultSet rs = null;
		
		int borcId = 0;
		
		try {
			psmt = conn.prepareStatement(query);
			psmt.setInt(1, borc.getBorcMiktari());
			psmt.setInt(2, borc.getBorcVade());
			psmt.setString(3, borc.getBorcTarihi());
			psmt.setInt(4, borc.getYurtFiyat().getYurtFiyatId());
			
			psmt.execute();
			
			String q = "select * from borc2";
			psmt = conn.prepareStatement(q);
			rs = psmt.executeQuery();
			int a = 0;
			while(rs.next()) {
				a = rs.getInt(1);
			}
			
			borcId = a;
			
			psmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return borcId;
	}
	
	public void dosyaYaz(String id) {
		File file = new File("C:\\Users\\Lenovo\\Desktop\\Hello1.txt");

		// creates the file
		try {
			file.createNewFile();
			// creates a FileWriter Object
			FileWriter writer = new FileWriter(file);

			// Writes the content to the file
			writer.write(id);
			writer.flush();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public void ogrenciEkle(Ogrenci3 ogrenci) {

		Connection conn = JDBCConnection.getConnection();
		PreparedStatement psmt = null;
		ResultSet rs = null;
		String query = "insert into ogrenci3(ad, soyad, tc, telefon, veliTelefon, kacinciSinif, odaId, borcId, kullaniciAdi, parola) values(?,?,?,?,?,?,?,?,?,?)";
		String query2 = "select * from ogrenci3 where kullaniciAdi=? and tc=?";
		
		try {
			psmt = conn.prepareStatement(query);
			psmt.setString(1, ogrenci.getAd());
			psmt.setString(2, ogrenci.getSoyad());
			psmt.setLong(3,ogrenci.getTc());
			psmt.setLong(4, ogrenci.getTelefon());
			psmt.setLong(5, ogrenci.getVeliTelefon());
			psmt.setInt(6, ogrenci.getKacinciSinif());
			psmt.setInt(7, ogrenci.getOda().getOdaId());
			psmt.setInt(8, ogrenci.getBorc().getBorcId());
			psmt.setString(9, ogrenci.getAd().toLowerCase() + "." + ogrenci.getSoyad().toLowerCase() + "@yurt.com");
			psmt.setString(10, ogrenci.getAd().toLowerCase());
			psmt.execute();
			
			psmt = conn.prepareStatement(query2);
			psmt.setString(1, ogrenci.getAd().toLowerCase() + "." + ogrenci.getSoyad().toLowerCase() + "@yurt.com");
			psmt.setLong(2, ogrenci.getTc());
			rs = psmt.executeQuery();
			rs.next();
			String id = rs.getString(1);
			System.out.println("Bas");
			dosyaYaz(id);
//			try {
//				Thread.sleep(3000);
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}
			
			psmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public Ogrenci3 getOgrenciId(int id){
		String sql = "select * from ogrenci3 where ogrenciId="+ id;
		String sql2 = "select * from oda where odaId=?";
		String sql3 = "select * from yurtfiyat where yurtfiyatId=?";
		String sql4 = "select * from borc2 where borcId=?";
		
		Connection conn = JDBCConnection.getConnection();
		PreparedStatement psmt = null;
		ResultSet rs, rs2, rs3, rs4 = null;
		
		Ogrenci3 o=null;
		
		try {
			psmt = conn.prepareStatement(sql);
			rs = psmt.executeQuery();
			
			if(rs.next()) {
				o = new Ogrenci3();
				o.setOgrenciId(id);
				o.setAd(rs.getString(2));
				o.setSoyad(rs.getString(3));
				o.setTc(rs.getLong(4));
				o.setTelefon(rs.getLong(5));
				o.setVeliTelefon(rs.getLong(6));
				o.setKacinciSinif(rs.getInt(7));
				o.setKullaniciAdi(rs.getString(10));
				
				Oda oda = new Oda();
				psmt = conn.prepareStatement(sql2);
				psmt.setInt(1, rs.getInt(8));
				rs2 = psmt.executeQuery();
				rs2.next();
				oda.setOdaId(rs2.getInt(1));
				oda.setOdaNo(rs2.getInt(2));
				oda.setOdaKapasite(rs2.getInt(3));
				
				o.setOda(oda);
				
				Borc borc = new Borc();
				psmt = conn.prepareStatement(sql4);
				psmt.setInt(1, rs.getInt(9));
				rs4 = psmt.executeQuery();
				rs4.next();
				borc.setBorcId(rs4.getInt(1));
				borc.setBorcMiktari(rs4.getInt(2));
				borc.setBorcVade(rs4.getInt(3));
				borc.setBorcTarihi(rs4.getString(4));
				
				
				YurtFiyat yf = new YurtFiyat();
				psmt = conn.prepareStatement(sql3);
				psmt.setInt(1, rs4.getInt(5));
				rs3 = psmt.executeQuery();
				rs3.next();
				yf.setYurtFiyatId(rs3.getInt(1));
				yf.setToplamTutar(rs3.getInt(2));
				yf.setOdaKapasitesi(rs3.getInt(3));
				
				borc.setYurtFiyat(yf);
				
				o.setBorc(borc);
				
			}
			
			psmt.close();
			conn.close();
		} catch (SQLException e ) {
			e.printStackTrace();
		}
		
		return o;
	}
	
	public void editOgrenci(Ogrenci3 o) {
		String query = "update ogrenci3 set ad=?, soyad=?, tc=?, telefon=?, veliTelefon=?, kacinciSinif=?, odaId=?, kullaniciAdi=?, parola=?" + " where ogrenciId=?";
		
		Connection conn = JDBCConnection.getConnection();
		PreparedStatement psmt = null;
		
		try {
			psmt = conn.prepareStatement(query);
			psmt.setString(1, o.getAd());
			psmt.setString(2, o.getSoyad());
			psmt.setLong(3, o.getTc());
			psmt.setLong(4, o.getTelefon());
			psmt.setLong(5, o.getVeliTelefon());
			psmt.setInt(6, o.getKacinciSinif());
			psmt.setInt(7, o.getOda().getOdaId());
			psmt.setString(8, o.getAd().toLowerCase()+"."+o.getSoyad().toLowerCase()+"@yurt.com");
			psmt.setString(9, o.getAd().toLowerCase());
			psmt.setInt(10, o.getOgrenciId());
			
			psmt.executeUpdate();
			
			psmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void delete(int id) {
		String query = "delete from ogrenci3 where ogrenciId=?";
		
		Connection conn = JDBCConnection.getConnection();
		PreparedStatement psmt = null;
		
		try {
			psmt = conn.prepareStatement(query);
			psmt.setInt(1, id);
			psmt.execute();
			
			psmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	public List<Oda> odaListele2() {

		List<Integer> odaKapasite = new ArrayList<>();
		List<Integer> ogrenciOdaId = new ArrayList<>();
		
		List<Oda> odaListesi = new ArrayList<>();
		
		Connection conn = JDBCConnection.getConnection();
		Connection conn2 = JDBCConnection.getConnection();
		
		PreparedStatement psmt, psmt2 = null;
		ResultSet rs, rs2 = null;

		String query = "select * from oda";
		
		String query2 = "select * from ogrenci3";

		try {
			psmt = conn.prepareStatement(query);
			rs = psmt.executeQuery();
			while (rs.next()) {
				odaKapasite.add(rs.getInt(3));
			}
			
			psmt = conn.prepareStatement(query2);
			rs = psmt.executeQuery();
			while(rs.next()) {
				ogrenciOdaId.add(rs.getInt(8));
			}
			psmt.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		int tampon=0;
		int lala = 0;
		
		HashSet<Integer> yasakliOdaId2 = new HashSet<>();
		
		lala = 0;
		for(int i = 0; i<ogrenciOdaId.size(); i++) {
			tampon = ogrenciOdaId.get(i);
			int counter = 0;
			for(int j = 0; j<ogrenciOdaId.size(); j++) {
				lala = ogrenciOdaId.get(j);
				if(tampon == lala) {
					counter++;
					int b = odaKapasite.get(tampon-1);
					if(counter == b) {
						yasakliOdaId2.add(tampon);
					}
				}
			}
		}
		
		Iterator<Integer> iter = yasakliOdaId2.iterator();
		List<Integer> yasakliOdaIdSon = new ArrayList<>();
		
		while(iter.hasNext()) {
			yasakliOdaIdSon.add(iter.next());
		}
		//yasakliOdaIdSon.forEach(item -> System.out.print(item + "  "));
		
		//String query3 = "select * from oda where odaId not in(?,?,?,?)";
		StringBuilder sb=new StringBuilder();
		sb.append("select * from oda where odaId not in(");
		
		String query4 = "";
		
		int counter = 0;
		
		if(yasakliOdaIdSon.size()>1) {
			for(int i=0; i<yasakliOdaIdSon.size();i++) {
				if(i==(yasakliOdaIdSon.size() - 1)) {
					sb.append("?)");
				}else {
					sb.append("?, ");
				}
			}
			query4 = sb.toString();
			
			counter = yasakliOdaIdSon.size();
		}else if(yasakliOdaIdSon.size()==1) {
			query4 = "select * from oda where odaId not in(?)";
			counter = 1;
		}else {
			query4 = "select * from oda";
			counter = 0;
		}
		
		try {
			psmt2 = conn2.prepareStatement(query4);
			if(counter==0) {
				rs2 = psmt2.executeQuery();
			}else if(counter >= 1) {
				for(int i=0; i<counter;i++) {
					psmt2.setInt(i+1, yasakliOdaIdSon.get(i));
				}
				rs2 = psmt2.executeQuery();
			}
			
			
			Oda o;
			while (rs2.next()) {
				o = new Oda();
				o.setOdaId(rs2.getInt(1));
				o.setOdaNo(rs2.getInt(2));
				o.setOdaKapasite(rs2.getInt(3));
				odaListesi.add(o);
			}
			psmt2.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return odaListesi;
	}
	
	public Oda getOdaId(int id){
		Oda oda = new Oda();
		String sql = "select * from oda where odaId=" + id;
		
		Connection conn = JDBCConnection.getConnection();
		PreparedStatement psmt = null;
		ResultSet rs = null;

		try {
			psmt = conn.prepareStatement(sql);
			rs = psmt.executeQuery();

			rs.next();
			
			oda.setOdaId(id);
			oda.setOdaNo(rs.getInt(2));
			oda.setOdaKapasite(rs.getInt(3));
			
			psmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return oda;
	}
	
	public Borc getBorcId(int id){
		Borc borc = new Borc();
		String sql = "select * from borc2 where borcId=" + id;
		String sql2 = "select * from yurtfiyat where yurtfiyatId=" + id;
		
		
		Connection conn = JDBCConnection.getConnection();
		PreparedStatement psmt = null;
		ResultSet rs, rs2 = null;

		try {
			psmt = conn.prepareStatement(sql);
			rs = psmt.executeQuery();

			rs.next();
			
			borc.setBorcId(id);
			borc.setBorcMiktari(rs.getInt(2));
			borc.setBorcVade(rs.getInt(3));
			borc.setBorcTarihi(rs.getString(4));
			
			YurtFiyat yf = new YurtFiyat();
			yf.setYurtFiyatId(rs.getInt(rs.getInt(5)));
			psmt = conn.prepareStatement(sql2);
			rs2 = psmt.executeQuery();
			rs2.next();
			yf.setToplamTutar(rs2.getInt(2));
			yf.setOdaKapasitesi(rs2.getInt(3));
			
			borc.setYurtFiyat(yf);
			
			psmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return borc;
	}
	
	public YurtFiyat getYurtFiyat(int odaKapasite){
		YurtFiyat yf = new YurtFiyat();
		String sql = "select * from yurtfiyat where odaKapasitesi=" + odaKapasite;
		
		Connection conn = JDBCConnection.getConnection();
		PreparedStatement psmt = null;
		ResultSet rs = null;

		try {
			psmt = conn.prepareStatement(sql);
			rs = psmt.executeQuery();

			rs.next();
			
			yf.setYurtFiyatId(rs.getInt(1));
			yf.setToplamTutar(rs.getInt(2));
			yf.setOdaKapasitesi(rs.getInt(3));
			
			psmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return yf;
	}
	
	
	public List<Disiplin> disiplinListele(){
		List<Disiplin> liste = new ArrayList<>();
		
		Connection conn = JDBCConnection.getConnection();
		PreparedStatement psmt = null;
		ResultSet rs= null;
	
		String query = "select * from disiplin2";
	
		try {
			psmt = conn.prepareStatement(query);
			rs = psmt.executeQuery();
			Disiplin disiplin = new Disiplin();
			
			while(rs.next()) {
				Disiplin d = (Disiplin)disiplin.clone();
				d.setDisiplinId(rs.getInt(1));
				d.setSebep(rs.getString(2));
				d.setCezasi(rs.getString(3));
				d.setOgrenciId(rs.getInt(4));
				
				liste.add(d);
			}
			
			psmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		
		return liste;
	}
	
	public List<Disiplin> getDisiplinId(int id){
		List<Disiplin> list = new ArrayList<>();
		String sql = "select * from disiplin2 where disiplinId="+ id;
		Connection conn = JDBCConnection.getConnection();
		PreparedStatement psmt = null;
		ResultSet rs = null;
		
		try {
			psmt = conn.prepareStatement(sql);
			rs = psmt.executeQuery();
			rs.next();
			
			Disiplin d = new Disiplin();
			d.setDisiplinId(rs.getInt(1));
			d.setSebep(rs.getString(2));
			d.setCezasi(rs.getString(3));
			d.setOgrenciId(rs.getInt(4));
			
			list.add(d);
			
			psmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return list;
	}
	
	public void disiplinEkle(Disiplin disiplin) {
		String query = "insert into disiplin2(sebep, cezasi, ogrenciId) values(?,?,?)";
		
		Connection conn = JDBCConnection.getConnection();
		PreparedStatement psmt = null;
		
		try {
			psmt = conn.prepareStatement(query);
			psmt.setString(1, disiplin.getSebep());
			psmt.setString(2, disiplin.getCezasi());
			psmt.setInt(3, disiplin.getOgrenciId());
			
			psmt.execute();
			
			psmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void editDisiplin(Disiplin d) {
		String query = "update disiplin2 set sebep=?, cezasi=?, ogrenciId=?" + " where disiplinId=?";
		Connection conn = JDBCConnection.getConnection();
		PreparedStatement psmt = null;
		
		try {
			psmt = conn.prepareStatement(query);
			psmt.setString(1, d.getSebep());
			psmt.setString(2, d.getCezasi());
			psmt.setInt(3, d.getOgrenciId());
			psmt.setInt(4, d.getDisiplinId());
			
			psmt.executeUpdate();
			
			psmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void disiplinSil(int id) {
		String query = "delete from disiplin2 where disiplinId=?";
		
		Connection conn = JDBCConnection.getConnection();
		PreparedStatement psmt = null;
		
		try {
			psmt = conn.prepareStatement(query);
			psmt.setInt(1, id);
			psmt.execute();
			
			psmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	public Object login(String username, String password) {

		Object a = null;
		Connection conn = JDBCConnection.getConnection();
		PreparedStatement psmt = null;
		ResultSet rs, rs2, rs3, rs4, rs5 = null;

		String query = "select * from ogrenci3 where kullaniciAdi=? and parola=?";
		
		String query2 = "select * from admin where kullaniciAdi=? and parola=? and gorevi='M�d�r'";
		
		String sql2 = "select * from oda where odaId=?";
		String sql3 = "select * from yurtfiyat where yurtfiyatId=?";
		String sql4 = "select * from borc2 where borcId=?";
		
		try {
			psmt = conn.prepareStatement(query);
			psmt.setString(1, username);
			psmt.setString(2, password);
			
			rs = psmt.executeQuery();
			
			if(rs.next()) {
				a = new Ogrenci3();
				((Ogrenci3) a).setOgrenciId(rs.getInt(1));
				((Ogrenci3) a).setAd(rs.getString(2));
				((Ogrenci3) a).setSoyad(rs.getString(3));
				((Ogrenci3) a).setTc(rs.getLong(4));
				((Ogrenci3) a).setTelefon(rs.getLong(5));
				((Ogrenci3) a).setVeliTelefon(rs.getLong(6));
				((Ogrenci3) a).setKacinciSinif(rs.getInt(7));
				
				Oda oda = new Oda();
				psmt = conn.prepareStatement(sql2);
				psmt.setInt(1, rs.getInt(8));
				rs2 = psmt.executeQuery();
				rs2.next();
				oda.setOdaId(rs2.getInt(1));
				oda.setOdaNo(rs2.getInt(2));
				oda.setOdaKapasite(rs2.getInt(3));
				
				((Ogrenci3) a).setOda(oda);
				
				Borc borc = new Borc();
				psmt = conn.prepareStatement(sql4);
				psmt.setInt(1, rs.getInt(9));
				rs4 = psmt.executeQuery();
				rs4.next();
				borc.setBorcId(rs4.getInt(1));
				borc.setBorcMiktari(rs4.getInt(2));
				borc.setBorcVade(rs4.getInt(3));
				borc.setBorcTarihi(rs4.getString(4));
				
				
				YurtFiyat yf = new YurtFiyat();
				psmt = conn.prepareStatement(sql3);
				psmt.setInt(1, rs4.getInt(5));
				rs3 = psmt.executeQuery();
				rs3.next();
				yf.setYurtFiyatId(rs3.getInt(1));
				yf.setToplamTutar(rs3.getInt(2));
				yf.setOdaKapasitesi(rs3.getInt(3));
				
				borc.setYurtFiyat(yf);
				
				((Ogrenci3) a).setBorc(borc);
				
				((Ogrenci3) a).setKullaniciAdi(rs.getString(10));
				((Ogrenci3) a).setParola(rs.getString(11));
				
			}else {
				psmt = conn.prepareStatement(query2);
				psmt.setString(1, username);
				psmt.setString(2, password);
				rs5 = psmt.executeQuery();
				if(rs5.next()) {
					a = new Admin();
					((Admin) a).setAdminId(rs5.getInt(1));
					((Admin) a).setAd(rs5.getString(2));
					((Admin) a).setSoyad(rs5.getString(3));
					((Admin) a).setGorevi(rs5.getString(4));
					((Admin) a).setKullaniciAdi(rs5.getString(5));
					((Admin) a).setParola(rs5.getString(6));
				}
			}
			
			psmt.close();
			conn.close();
			
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		return a;
	}
	
	
}
