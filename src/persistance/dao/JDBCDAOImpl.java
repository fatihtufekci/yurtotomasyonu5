package persistance.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import model.Admin;
import model.Disiplin;
import model.Duyuru;
import model.Oda;
import model.Ogrenci;
import util.JDBCConnection;

public class JDBCDAOImpl implements DAO {

	@Override
	public Admin check(Admin admin) {

		Admin a = null;
		Connection conn = JDBCConnection.getConnection();
		PreparedStatement psmt = null;
		ResultSet rs = null;

		String query = "select * from admin where username=? and password=?";

		String query2 = "select * from admin where gorevi='M�d�r'";

		String query3 = "select * from admin where gorevi='�dari M�d�r Yard�mc�s�'";

		String query4 = "select * from admin where gorevi='��renci M�d�r Yard�mc�s�'";

		try {
			psmt = conn.prepareStatement(query2);
			rs = psmt.executeQuery();
			String ad, soyad, gorevi, kullaniciAdi = "", parola = "";
			
			rs.next();
			
			a = new Admin();
			ad = rs.getString(2);
			soyad = rs.getString(3);
			gorevi = rs.getString(4);
			kullaniciAdi = rs.getString(5);
			parola = rs.getString(6);

			a.setAd(ad);
			a.setSoyad(soyad);
			a.setGorevi(gorevi);
			a.setKullaniciAdi(kullaniciAdi);
			a.setParola(parola);

			if (!(admin.getKullaniciAdi().equals(kullaniciAdi) && admin.getParola().equals(parola))) {
				a = null;
			}
			
			psmt.close();
			conn.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return a;
	}

	@Override
	public List<Oda> odaListele() {

		List<Oda> liste = new ArrayList<>();
		Connection conn = JDBCConnection.getConnection();
		PreparedStatement psmt = null;
		ResultSet rs = null;

		String query = "select * from oda";

		try {
			psmt = conn.prepareStatement(query);
			rs = psmt.executeQuery();
			Oda o;
			while (rs.next()) {
				o = new Oda();
				o.setOdaNo(Integer.parseInt(rs.getString(2)));
				o.setOdaKapasite(Integer.parseInt(rs.getString(3)));
				liste.add(o);
			}
			
			psmt.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return liste;
	}
	
	
	public List<Oda> odaListele2() {

		List<Integer> odaKapasite = new ArrayList<>();
		List<Integer> ogrenciOdaId = new ArrayList<>();
		
		List<Oda> odaListesi = new ArrayList<>();
		
		Connection conn = JDBCConnection.getConnection();
		Connection conn2 = JDBCConnection.getConnection();
		
		PreparedStatement psmt, psmt2 = null;
		ResultSet rs, rs2 = null;

		String query = "select * from oda";
		
		String query2 = "select * from ogrenci";

		try {
			psmt = conn.prepareStatement(query);
			rs = psmt.executeQuery();
			while (rs.next()) {
				odaKapasite.add(rs.getInt(3));
			}
			
			psmt = conn.prepareStatement(query2);
			rs = psmt.executeQuery();
			while(rs.next()) {
				ogrenciOdaId.add(rs.getInt(8));
			}
			psmt.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		int tampon=0;
		int lala = 0;
		
		HashSet<Integer> yasakliOdaId2 = new HashSet<>();
		
		lala = 0;
		for(int i = 0; i<ogrenciOdaId.size(); i++) {
			tampon = ogrenciOdaId.get(i);
			int counter = 0;
			for(int j = 0; j<ogrenciOdaId.size(); j++) {
				lala = ogrenciOdaId.get(j);
				if(tampon == lala) {
					counter++;
					int b = odaKapasite.get(tampon-1);
					if(counter == b) {
						yasakliOdaId2.add(tampon);
					}
				}
			}
		}
		
		Iterator<Integer> iter = yasakliOdaId2.iterator();
		List<Integer> yasakliOdaIdSon = new ArrayList<>();
		
		while(iter.hasNext()) {
			yasakliOdaIdSon.add(iter.next());
		}
		//yasakliOdaIdSon.forEach(item -> System.out.print(item + "  "));
		
		//String query3 = "select * from oda where odaId not in(?,?,?,?)";
		StringBuilder sb=new StringBuilder();
		sb.append("select * from oda where odaId not in(");
		
		String query4 = "";
		
		int counter = 0;
		
		if(yasakliOdaIdSon.size()>1) {
			for(int i=0; i<yasakliOdaIdSon.size();i++) {
				if(i==(yasakliOdaIdSon.size() - 1)) {
					sb.append("?)");
				}else {
					sb.append("?, ");
				}
			}
			query4 = sb.toString();
			
			counter = yasakliOdaIdSon.size();
		}else if(yasakliOdaIdSon.size()==1) {
			query4 = "select * from oda where odaId not in(?)";
			counter = 1;
		}else {
			query4 = "select * from oda";
			counter = 0;
		}
		
		try {
			psmt2 = conn2.prepareStatement(query4);
			if(counter==0) {
				rs2 = psmt2.executeQuery();
			}else if(counter >= 1) {
				for(int i=0; i<counter;i++) {
					psmt2.setInt(i+1, yasakliOdaIdSon.get(i));
				}
				rs2 = psmt2.executeQuery();
			}
			
			
			Oda o;
			while (rs2.next()) {
				o = new Oda();
				o.setOdaNo(Integer.parseInt(rs2.getString(2)));
				o.setOdaKapasite(Integer.parseInt(rs2.getString(3)));
				odaListesi.add(o);
			}
			psmt2.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return odaListesi;
	}
	
	public List<Disiplin> disiplinListele(){
		List<Disiplin> liste = new ArrayList<>();
		
		Connection conn = JDBCConnection.getConnection();
		PreparedStatement psmt = null;
		ResultSet rs= null;
	
		String query = "select * from disiplin";
	
		try {
			psmt = conn.prepareStatement(query);
			rs = psmt.executeQuery();
			Disiplin disiplin = new Disiplin();
			
			while(rs.next()) {
				Disiplin d = (Disiplin)disiplin.clone();
				d.setDisiplinId(rs.getInt(1));
				d.setSebep(rs.getString(2));
				d.setCezasi(rs.getString(3));
				d.setOgrenciId(rs.getInt(4));
				
				liste.add(d);
			}
			
			psmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		
		return liste;
	}
	public List<Duyuru> duyuruListele(){
		List<Duyuru> liste = new ArrayList<>();

		Connection conn = JDBCConnection.getConnection();
		PreparedStatement psmt = null;
		ResultSet rs= null;
	
		String query = "select * from duyurular";
		
		try {
			psmt = conn.prepareStatement(query);
			rs = psmt.executeQuery();
			Duyuru duyuru = new Duyuru();
			while(rs.next()) {
				Duyuru d = (Duyuru)duyuru.clone();
				d.setDuyuruId(rs.getInt(1));
				d.setDuyuruKonu(rs.getString(2));
				d.setDuyuruIcerik(rs.getString(3));
				d.setDuyuruTarih(rs.getString(4));
				
				liste.add(d);
			}
			
			psmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		
		return liste;
	}
	
	@Override
	public List<Ogrenci> ogrenciListele() {

		List<Ogrenci> liste = new ArrayList<>();

		Connection conn = JDBCConnection.getConnection();
		PreparedStatement psmt = null;
		ResultSet rs, rs2 = null;

		String query = "select * from ogrenci";
		String query2 = "select odaNo from oda where odaNo=?";

		try {
			psmt = conn.prepareStatement(query);
			rs = psmt.executeQuery();
			Ogrenci o1 = new Ogrenci();
			while (rs.next()) {
//	        	o = new Ogrenci();
				Ogrenci o = (Ogrenci) o1.clone();
				o.setOgrenciId(rs.getInt(1));
				o.setAd(rs.getString(2));
				o.setSoyad(rs.getString(3));
				o.setTc(rs.getLong(4));
				o.setTelefon(rs.getLong(5));
				o.setVeliTelefon(rs.getLong(6));
				o.setKacinciSinif(rs.getInt(7));
				psmt = conn.prepareStatement(query2);
				psmt.setString(1, rs.getString(8));
				rs2 = psmt.executeQuery();
				rs2.next();
				o.setOdaNo(rs2.getInt(1));
				o.setBorcMiktari(rs.getInt(10));
				liste.add(o);
			}
			
			psmt.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return liste;
	}
	
	
	
	
	@Override
	public boolean ogrenciEkle(Ogrenci ogrenci) {

		Connection conn = JDBCConnection.getConnection();
		PreparedStatement psmt = null;
		String query = "insert into ogrenci(ad, soyad, tc, telefon, veliTelefon, kacinciSinif, odaId, borc_miktari, kullaniciAdi, parola) values(?,?,?,?,?,?,?,?,?,?)";

		try {
			psmt = conn.prepareStatement(query);
			psmt.setString(1, ogrenci.getAd());
			psmt.setString(2, ogrenci.getSoyad());
			psmt.setString(3, String.valueOf(ogrenci.getTc()));
			psmt.setString(4, String.valueOf(ogrenci.getTelefon()));
			psmt.setString(5, String.valueOf(ogrenci.getVeliTelefon()));
			psmt.setString(6, String.valueOf(ogrenci.getKacinciSinif()));
			psmt.setString(7, String.valueOf(ogrenci.getOdaNo()));
			psmt.setString(8, String.valueOf(0));

			psmt.setString(9, ogrenci.getAd() + "." + ogrenci.getSoyad() + "@yurt.std.tr");
			psmt.setString(10, ogrenci.getAd());
			psmt.execute();
			
			psmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}
	
	public List<Oda> getOda(int id){
		List<Oda> list = new ArrayList<>();
		String sql = "select * from Oda where odaId=" + id;
		Connection conn = JDBCConnection.getConnection();
		PreparedStatement psmt = null;
		ResultSet rs = null;
		
		try {
			psmt = conn.prepareStatement(sql);
			rs = psmt.executeQuery();
			
			Oda oda = new Oda();
			
			while(rs.next()) {
				oda.setOdaNo(rs.getInt(2));
				oda.setOdaKapasite(rs.getInt(3));
				list.add(oda);
			}
			psmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
	
	public List<Duyuru> getDuyuruId(int id){
		List<Duyuru> list = new ArrayList<>();
		String sql = "select * from duyurular where duyuruId="+ id;
		Connection conn = JDBCConnection.getConnection();
		PreparedStatement psmt = null;
		ResultSet rs = null;
		
		try {
			psmt = conn.prepareStatement(sql);
			rs = psmt.executeQuery();
			rs.next();
			
			Duyuru d = new Duyuru();
			d.setDuyuruId(rs.getInt(1));
			d.setDuyuruKonu(rs.getString(2));
			d.setDuyuruIcerik(rs.getString(3));
			d.setDuyuruTarih(rs.getString(4));
			
			list.add(d);
			
			psmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return list;
	}
	
	public List<Disiplin> getDisiplinId(int id){
		List<Disiplin> list = new ArrayList<>();
		String sql = "select * from disiplin where disiplinId="+ id;
		Connection conn = JDBCConnection.getConnection();
		PreparedStatement psmt = null;
		ResultSet rs = null;
		
		try {
			psmt = conn.prepareStatement(sql);
			rs = psmt.executeQuery();
			rs.next();
			
			Disiplin d = new Disiplin();
			d.setDisiplinId(rs.getInt(1));
			d.setSebep(rs.getString(2));
			d.setCezasi(rs.getString(3));
			d.setOgrenciId(rs.getInt(4));
			
			list.add(d);
			
			psmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return list;
	}
	
	public List<Ogrenci> getById(int id){
		
		List<Ogrenci> list = new ArrayList<>();
		String sql = "select * from ogrenci where ogrenciId="+ id;
		
		Connection conn = JDBCConnection.getConnection();
		PreparedStatement psmt = null;
		ResultSet rs = null;
		
		try {
			psmt = conn.prepareStatement(sql);
			rs = psmt.executeQuery();
			Ogrenci o1 = new Ogrenci();
			while(rs.next()) {
				Ogrenci o = (Ogrenci) o1.clone();
				o.setOgrenciId(id);
				o.setAd(rs.getString(2));
				o.setSoyad(rs.getString(3));
				o.setTc(rs.getLong(4));
				o.setTelefon(rs.getLong(5));
				o.setVeliTelefon(rs.getLong(6));
				o.setKacinciSinif(rs.getInt(7));
				o.setOdaNo(rs.getInt(8));
				o.setBorcMiktari(rs.getInt(10));
				o.setKullaniciAdi(rs.getString(11));
				list.add(o);
			}
			psmt.close();
			conn.close();
		} catch (SQLException e ) {
			e.printStackTrace();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		
		return list;
	}
	
	public void disiplinEkle(Disiplin disiplin) {
		String query = "insert into disiplin(sebep, cezasi, ogrenciId) values(?,?,?)";
		
		Connection conn = JDBCConnection.getConnection();
		PreparedStatement psmt = null;
		
		try {
			psmt = conn.prepareStatement(query);
			psmt.setString(1, disiplin.getSebep());
			psmt.setString(2, disiplin.getCezasi());
			psmt.setInt(3, disiplin.getOgrenciId());
			
			psmt.execute();
			
			psmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void duyuruEkle(Duyuru duyuru) {
		String query = "insert into duyurular(duyuruKonu, duyuruIcerik, duyuruTarih) values(?,?,?)";
		
		Connection conn = JDBCConnection.getConnection();
		PreparedStatement psmt = null;
		
		try {
			psmt = conn.prepareStatement(query);
			psmt.setString(1, duyuru.getDuyuruKonu());
			psmt.setString(2, duyuru.getDuyuruIcerik());
			psmt.setString(3, duyuru.getDuyuruTarih());
			
			psmt.execute();
			
			psmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void editDuyuru(Duyuru d) {
		String query = "update duyurular set duyuruKonu=?, duyuruIcerik=?, duyuruTarih=?" + " where duyuruId=?";
		Connection conn = JDBCConnection.getConnection();
		PreparedStatement psmt = null;
		
		try {
			psmt = conn.prepareStatement(query);
			psmt.setString(1, d.getDuyuruKonu());
			psmt.setString(2, d.getDuyuruIcerik());
			psmt.setString(3, d.getDuyuruTarih());
			psmt.setInt(4, d.getDuyuruId());
			
			psmt.executeUpdate();
			
			psmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void editDisiplin(Disiplin d) {
		String query = "update disiplin set sebep=?, cezasi=?, ogrenciId=?" + " where disiplinId=?";
		Connection conn = JDBCConnection.getConnection();
		PreparedStatement psmt = null;
		
		try {
			psmt = conn.prepareStatement(query);
			psmt.setString(1, d.getSebep());
			psmt.setString(2, d.getCezasi());
			psmt.setInt(3, d.getOgrenciId());
			psmt.setInt(4, d.getDisiplinId());
			
			psmt.executeUpdate();
			
			psmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void edit(Ogrenci o) {
		String query = "update ogrenci set ad=?, soyad=?, tc=?, telefon=?, veliTelefon=?, kacinciSinif=?, odaId=?" + " where ogrenciId=?";
		
		Connection conn = JDBCConnection.getConnection();
		PreparedStatement psmt = null;
		
		try {
			psmt = conn.prepareStatement(query);
			psmt.setString(1, o.getAd());
			psmt.setString(2, o.getSoyad());
			psmt.setString(3, String.valueOf(o.getTc()));
			psmt.setString(4, String.valueOf(o.getTelefon()));
			psmt.setString(5, String.valueOf(o.getVeliTelefon()));
			psmt.setInt(6, o.getKacinciSinif());
			psmt.setInt(7, o.getOdaNo());
			psmt.setInt(8, o.getOgrenciId());
			
			psmt.executeUpdate();
			
			psmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void delete(int id) {
		String query = "delete from ogrenci where ogrenciId=?";
		
		Connection conn = JDBCConnection.getConnection();
		PreparedStatement psmt = null;
		
		try {
			psmt = conn.prepareStatement(query);
			psmt.setInt(1, id);
			psmt.execute();
			
			psmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void duyuruSil(int id) {
		String query = "delete from duyurular where duyuruId=?";
		
		Connection conn = JDBCConnection.getConnection();
		PreparedStatement psmt = null;
		
		try {
			psmt = conn.prepareStatement(query);
			psmt.setInt(1, id);
			psmt.execute();
			
			psmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void disiplinSil(int id) {
		String query = "delete from disiplin where disiplinId=?";
		
		Connection conn = JDBCConnection.getConnection();
		PreparedStatement psmt = null;
		
		try {
			psmt = conn.prepareStatement(query);
			psmt.setInt(1, id);
			psmt.execute();
			
			psmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
}
