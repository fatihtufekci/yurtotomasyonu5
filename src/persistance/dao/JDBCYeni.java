package persistance.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import util.JDBCConnection;

public class JDBCYeni {

	public int kasaParaGetir() {

		int bakiye = 0;

		Connection conn = JDBCConnection.getConnection();
		PreparedStatement psmt = null;
		ResultSet rs = null;
		String query = "select * from kasa";
		try {
			psmt = conn.prepareStatement(query);
			rs = psmt.executeQuery();
			rs.next();
			bakiye = rs.getInt(2);

			psmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return bakiye;
	}

	public boolean odemeYap(int giderId) {

		Connection conn = JDBCConnection.getConnection();
		PreparedStatement psmt = null;
		ResultSet rs, rs2, rs3 = null;
		String query = "select * from kasa";
		String query2 = "select * from giderler where giderId=" + giderId;
		String query3 = "update giderler set odemeYapildi=?" + " where giderId=?";
		String query4 = "update kasa set bakiye=?" + " where kasaId=1";

		int bakiye = 0;
		int gider = 0;
		try {
			psmt = conn.prepareStatement(query);
			rs = psmt.executeQuery();
			rs.next();
			bakiye = rs.getInt(2);

			psmt = conn.prepareStatement(query2);
			rs2 = psmt.executeQuery();
			rs2.next();
			gider = rs2.getInt(2) + rs2.getInt(3) + rs2.getInt(4) + rs2.getInt(5) + rs2.getInt(6);

			if (bakiye < gider) {
				return false;
			}

			psmt = conn.prepareStatement(query3);
			psmt.setInt(1, 1);
			psmt.setInt(2, giderId);
			psmt.executeUpdate();

			psmt = conn.prepareStatement(query4);
			psmt.setInt(1, bakiye - gider);
			psmt.executeUpdate();

			psmt.close();
			conn.close();

			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}

}
