package persistance.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Disiplin;
import model.Duyuru;
import model.Ogrenci;
import util.JDBCConnection;

public class JDBCDAOImplOgrenci {

	public Ogrenci getOgrenci(int id) {
		Ogrenci o = new Ogrenci();

		String sql = "select * from ogrenci where ogrenciId=" + id;

		Connection conn = JDBCConnection.getConnection();
		PreparedStatement psmt = null;
		ResultSet rs = null;

		try {
			psmt = conn.prepareStatement(sql);
			rs = psmt.executeQuery();
			Ogrenci o1 = new Ogrenci();
			rs.next();
			o.setOgrenciId(id);
			o.setAd(rs.getString(2));
			o.setSoyad(rs.getString(3));
			o.setTc(rs.getLong(4));
			o.setTelefon(rs.getLong(5));
			psmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return o;
	}
	
	public List<Duyuru> duyuruListele(){
		List<Duyuru> liste = new ArrayList<>();

		Connection conn = JDBCConnection.getConnection();
		PreparedStatement psmt = null;
		ResultSet rs= null;
	
		String query = "select * from duyurular";
		
		try {
			psmt = conn.prepareStatement(query);
			rs = psmt.executeQuery();
			Duyuru duyuru = new Duyuru();
			while(rs.next()) {
				Duyuru d = (Duyuru)duyuru.clone();
				d.setDuyuruId(rs.getInt(1));
				d.setDuyuruKonu(rs.getString(2));
				d.setDuyuruIcerik(rs.getString(3));
				d.setDuyuruTarih(rs.getString(4));
				
				liste.add(d);
			}
			
			psmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		
		return liste;
	}
	
	public void ogrenciGuncelle(Ogrenci o) {
		String query = "update ogrenci set ad=?, soyad=?, tc=?, telefon=?" + " where ogrenciId=?";
		
		Connection conn = JDBCConnection.getConnection();
		PreparedStatement psmt = null;
		
		try {
			psmt = conn.prepareStatement(query);
			psmt.setString(1, o.getAd());
			psmt.setString(2, o.getSoyad());
			psmt.setString(3, String.valueOf(o.getTc()));
			psmt.setString(4, String.valueOf(o.getTelefon()));
			psmt.setInt(5, o.getOgrenciId());
			
			psmt.executeUpdate();
			
			psmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public List<Disiplin> disiplinGetir(int id){
		List<Disiplin> liste = new ArrayList<>();
		
		String query = "select * from disiplin where ogrenciId=" + id;
		Connection conn = JDBCConnection.getConnection();
		PreparedStatement psmt = null;
		ResultSet rs = null;
		
		try {
			psmt = conn.prepareStatement(query);
			rs = psmt.executeQuery();
			while(rs.next()) {
				Disiplin d = new Disiplin();
				d.setSebep(rs.getString(2));
				d.setCezasi(rs.getString(3));
				liste.add(d);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
		return liste;
	}

}
