package persistance.dao;

import java.util.ResourceBundle;

import model.Admin;

public class DAOFactory {
	public static DAOFactory factory;

	private DAO dao = null;

	private DAOFactory() {
		initFactory();
	}

	public static DAOFactory instance() {
		if (factory == null) {
			factory = new DAOFactory();
		}
		return factory;
	}

	private void initFactory() {
		try {
			final String key = ResourceBundle.getBundle("properties/persistance").getString("dao");
			dao = (DAO) Class.forName(key).newInstance();
		} catch (final Exception e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
	}

	private DAO getDao() {
		return dao;
	}

	public Admin check(final Admin admin){
		return getDao().check(admin);
	}
}
