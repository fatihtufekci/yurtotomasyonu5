package persistance.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import model.Disiplin;
import model.Duyuru;
import model.Ogrenci3;
import util.JDBCConnection;

public class JDBCOgrenci3Impl {

	
	public Ogrenci3 getOgrenci(int id) {
		Ogrenci3 o = new Ogrenci3();

		String sql = "select * from ogrenci3 where ogrenciId=" + id;

		Connection conn = JDBCConnection.getConnection();
		PreparedStatement psmt = null;
		ResultSet rs = null;

		try {
			psmt = conn.prepareStatement(sql);
			rs = psmt.executeQuery();
			Ogrenci3 o1 = new Ogrenci3();
			rs.next();
			o.setOgrenciId(id);
			o.setAd(rs.getString(2));
			o.setSoyad(rs.getString(3));
			o.setTc(rs.getLong(4));
			o.setTelefon(rs.getLong(5));
			psmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return o;
	}
	
	public List<Duyuru> duyuruListele(){
		List<Duyuru> liste = new ArrayList<>();

		Connection conn = JDBCConnection.getConnection();
		PreparedStatement psmt = null;
		ResultSet rs= null;
	
		String query = "select * from duyurular";
		
		try {
			psmt = conn.prepareStatement(query);
			rs = psmt.executeQuery();
			Duyuru duyuru = new Duyuru();
			while(rs.next()) {
				Duyuru d = (Duyuru)duyuru.clone();
				d.setDuyuruId(rs.getInt(1));
				d.setDuyuruKonu(rs.getString(2));
				d.setDuyuruIcerik(rs.getString(3));
				d.setDuyuruTarih(rs.getString(4));
				
				liste.add(d);
			}
			
			psmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		
		return liste;
	}
	
	public void ogrenciGuncelle(Ogrenci3 o) {
		String query = "update ogrenci3 set ad=?, soyad=?, tc=?, telefon=?" + " where ogrenciId=?";
		
		Connection conn = JDBCConnection.getConnection();
		PreparedStatement psmt = null;
		
		try {
			psmt = conn.prepareStatement(query);
			psmt.setString(1, o.getAd());
			psmt.setString(2, o.getSoyad());
			psmt.setString(3, String.valueOf(o.getTc()));
			psmt.setString(4, String.valueOf(o.getTelefon()));
			psmt.setInt(5, o.getOgrenciId());
			
			psmt.executeUpdate();
			
			psmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public List<Disiplin> disiplinGetir(int id){
		List<Disiplin> liste = new ArrayList<>();
		
		String query = "select * from disiplin2 where ogrenciId=" + id;
		Connection conn = JDBCConnection.getConnection();
		PreparedStatement psmt = null;
		ResultSet rs = null;
		
		try {
			psmt = conn.prepareStatement(query);
			rs = psmt.executeQuery();
			while(rs.next()) {
				Disiplin d = new Disiplin();
				d.setSebep(rs.getString(2));
				d.setCezasi(rs.getString(3));
				liste.add(d);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
		return liste;
	}
	
	public void borcGuncelle() {
		Connection conn = JDBCConnection.getConnection();
		PreparedStatement psmt = null;
		ResultSet rs, rs2 = null;
		String query = "select * from borc2";
		String query2 = "update borc2 set borcVade=? where borcId=?";

		try {
			psmt = conn.prepareStatement(query);
			rs = psmt.executeQuery();
			while (rs.next()) {
				int borcId = rs.getInt(1);
				String tarih = rs.getString(4);
				String tarihAy = tarih.substring(5, 7);
				int borcVade = rs.getInt(3);
				int borcMiktari = rs.getInt(2);

				if (borcVade > 0) {
					String pattern = "yyyy-MM-dd";
					SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
					String date = simpleDateFormat.format(new Date());
					String bugun = date.substring(5, 7);
					int fark = Integer.parseInt(bugun) - Integer.parseInt(tarihAy);
					borcVade = borcVade - fark;

					psmt = conn.prepareStatement(query2);
					psmt.setInt(2, borcId);
					psmt.setInt(1, borcVade);

					psmt.executeUpdate();
				}
			}
			psmt.close();
			conn.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void duyuruGuncelle() {
		Connection conn = JDBCConnection.getConnection();
		PreparedStatement psmt = null;
		ResultSet rs, rs2 = null;
		String query = "select * from duyurular";
		String query2 = "delete from duyurular where duyuruId=?";
		
		try {
			psmt = conn.prepareStatement(query);
			rs = psmt.executeQuery();
			while(rs.next()) {
				int duyuruId = rs.getInt(1);
				String duyuruTarih = rs.getString(4);
				
				String pattern = "yyyy-MM-dd";
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
				String date = simpleDateFormat.format(new Date());
				
				Date bugun = simpleDateFormat.parse(date);
				Date son = simpleDateFormat.parse(duyuruTarih);
				
				long diffInMillies = son.getTime() - bugun.getTime();
			    long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
				
			    String ss = String.valueOf(diff);
			    if(ss.charAt(0)=='-') {
			    	
			    	psmt = conn.prepareStatement(query2);
			    	psmt.setInt(1, duyuruId);
			    	psmt.executeUpdate();
			    	
			    }
			    
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
	}

	
}
