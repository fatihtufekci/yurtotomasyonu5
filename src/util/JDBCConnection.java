package util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JDBCConnection {
	
	public static synchronized Connection getConnection(){
		Connection connection = null;
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/yurt?useSSL=false", "root", "my1234");
		} catch (ClassNotFoundException e) {
			System.out.println("com.mysql.jdbc.Driver" + " bulunamad�. " + e.getMessage());
		} catch (SQLException e) {
			System.out.println("Connection al�rken problem olu�tu. " + e.getMessage());
		}
		
		return connection;
	}
	
	public static void main(String[] args) {
		Connection conn = getConnection();
		if (conn != null)
			System.out.println(conn);
		else
			System.out.println("Ba�lant� yok!");
	}
}
