package duzenle.ogrenci;

import java.io.FileOutputStream;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;

import model.Ogrenci3;
import persistance.dao.JDBCOgrenci3;

public class OgrenciExcelCikar {
	
	public void writeExcel() {
		
		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet sheet = workbook.createSheet("Ogrenci Listesi");
		
		Row rowHeading = sheet.createRow(0);
		rowHeading.createCell(0).setCellValue("Ad");
		rowHeading.createCell(1).setCellValue("Soyad");
		rowHeading.createCell(2).setCellValue("TC");
		rowHeading.createCell(3).setCellValue("Telefon");
		rowHeading.createCell(4).setCellValue("Veli Telefon");
		rowHeading.createCell(5).setCellValue("S�n�f�");
		rowHeading.createCell(6).setCellValue("Oda No");
		rowHeading.createCell(7).setCellValue("Oda Kapasite");	
		rowHeading.createCell(8).setCellValue("Bor� Vadesi");	
		rowHeading.createCell(9).setCellValue("Ayl�k Borcu");	
		rowHeading.createCell(10).setCellValue("Kullan�c� Ad�");	
		rowHeading.createCell(11).setCellValue("Kart ID");	
		
		for(int i=0; i<12; i++) {
			CellStyle stylerowHeading = workbook.createCellStyle();
			Font font = workbook.createFont();
			font.setBold(true);
			font.setFontHeightInPoints((short)11);
			stylerowHeading.setFont(font);
			rowHeading.getCell(i).setCellStyle(stylerowHeading);
		}
		
		JDBCOgrenci3 j = new JDBCOgrenci3();
		
		int r = 1;
		for(Ogrenci3 ogr: j.ogrenciListele()) {
			
			Row row = sheet.createRow(r);
			//Ad Column
			Cell cellAd = row.createCell(0);
			cellAd.setCellValue(ogr.getAd());
			
			//Soyad
			Cell cellSoyad = row.createCell(1);
			cellSoyad.setCellValue(ogr.getSoyad());
			
			//TC
			Cell cellTc = row.createCell(2);
			cellTc.setCellValue(ogr.getTc());
			
			//Telefon
			Cell cellTel = row.createCell(3);
			cellTel.setCellValue(ogr.getTelefon());
			
			//VeliTelefon
			Cell cellVeliTel = row.createCell(4);
			cellVeliTel.setCellValue(ogr.getVeliTelefon());
			
			//S�n�f�
			Cell cellSinif = row.createCell(5);
			cellSinif.setCellValue(ogr.getKacinciSinif());
			
			//Oda No
			Cell cellOdaNo = row.createCell(6);
			cellOdaNo.setCellValue(ogr.getOda().getOdaNo());
			
			//Oda Kapasite
			Cell cellKapasite = row.createCell(7);
			cellKapasite.setCellValue(ogr.getOda().getOdaKapasite());
			
			//Bor� Vadesi
			Cell borcVadesi = row.createCell(8);
			borcVadesi.setCellValue(ogr.getBorc().getBorcVade());
			
			//Ayl�k Bor�
			Cell cellborc = row.createCell(9);
			cellborc.setCellValue(ogr.getBorc().getBorcMiktari());
			
			//Kullan�c� Ad�
			Cell cellKullaniciAdi = row.createCell(10);
			cellKullaniciAdi.setCellValue(ogr.getKullaniciAdi());
			
			//Kart ID
			Cell cellKartID = row.createCell(11);
			cellKartID.setCellValue(ogr.getKartId());
			
			r++;
		}
		
		
		for(int i=0; i<6; i++) {
			sheet.autoSizeColumn(i);
		}
		
		try {
			FileOutputStream out = new FileOutputStream("C:\\Users\\Lenovo\\Desktop\\ogrenciList.xls");
			workbook.write(out);
			out.close();
			workbook.close();
			System.out.println("Excel written successfully");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
	
}
