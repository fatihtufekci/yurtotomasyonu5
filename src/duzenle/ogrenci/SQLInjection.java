package duzenle.ogrenci;

public class SQLInjection {
	
	public boolean duzgunVeri(String username, String password) {
		
		if(username.contains("\\") || username.contains("\"") || username.contains("<") || username.contains(">") || username.contains("=") ||
				username.contains("'") || username.contains("/") || username.contains("=") || username.contains(",") || username.contains("-")) {
			return false;
		}else if(password.contains("\\") || password.contains("\"") || password.contains("<") || password.contains(">") || password.contains("=") ||
				password.contains("'") || password.contains("/") || password.contains("=") || password.contains(",")){
			return false;
		}else {
			return true;
		}
	}
	
}
