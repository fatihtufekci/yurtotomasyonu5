package business.facade;

import model.Admin;

public interface BusinessFacade {
	
	Admin loginCheck(Admin admin);
	
}
