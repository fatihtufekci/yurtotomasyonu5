package business.facade;

import business.service.ServiceFactory;
import model.Admin;

public class BusinessFacadeImpl implements BusinessFacade{
	
	@Override
	public Admin loginCheck(Admin admin) {
		final Admin result = ServiceFactory.instance().loginCheck(admin);
		return result;
	}

}
