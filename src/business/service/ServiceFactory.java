package business.service;

import java.util.ResourceBundle;

import model.Admin;

public class ServiceFactory{

	public static ServiceFactory factory;

	private BusinessService service = null;

	private ServiceFactory() {
		initFactory();
	}

	public static ServiceFactory instance() {
		if (factory == null) {
			factory = new ServiceFactory();
		}
		return factory;
	}

	private void initFactory() {
		try {
			final String facade = ResourceBundle.getBundle("properties/service").getString("businessservice");
			service = (BusinessService) Class.forName(facade).newInstance();
		} catch (final Exception e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
	}

	public Admin loginCheck(final Admin hotel){
		return getBusinessService().loginCheck(hotel);
	}

	private BusinessService getBusinessService() {
		return service;
	}

}
