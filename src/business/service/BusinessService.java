package business.service;

import model.Admin;

public interface BusinessService {
	
	Admin loginCheck(Admin admin);
	
}
