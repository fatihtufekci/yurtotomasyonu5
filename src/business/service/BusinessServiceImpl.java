package business.service;

import model.Admin;
import persistance.dao.DAOFactory;

public class BusinessServiceImpl implements BusinessService{

	@Override
	public Admin loginCheck(Admin admin) {
		
		Admin a = null;
		
		a = DAOFactory.instance().check(admin);
		
		return a;
	}

}
