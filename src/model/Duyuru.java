package model;

public class Duyuru implements Cloneable{
	
	private int duyuruId;
	private String duyuruKonu;
	private String duyuruIcerik;
	private String duyuruTarih;
	
	
	public Duyuru() { }

	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
	
	public int getDuyuruId() {
		return duyuruId;
	}


	public void setDuyuruId(int duyuruId) {
		this.duyuruId = duyuruId;
	}


	public String getDuyuruKonu() {
		return duyuruKonu;
	}


	public void setDuyuruKonu(String duyuruKonu) {
		this.duyuruKonu = duyuruKonu;
	}


	public String getDuyuruIcerik() {
		return duyuruIcerik;
	}


	public void setDuyuruIcerik(String duyuruIcerik) {
		this.duyuruIcerik = duyuruIcerik;
	}


	public String getDuyuruTarih() {
		return duyuruTarih;
	}


	public void setDuyuruTarih(String duyuruTarih) {
		this.duyuruTarih = duyuruTarih;
	}
	
}
