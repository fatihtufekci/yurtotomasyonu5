package model;

public class Gider implements Cloneable{
	
	private int giderId;
	private int elektrik;
	private int su;
	private int dogalgaz;
	private int maaslar;
	private int yemekGideri;
	private String giderTarihi;
	private int odemeYapildi;
	
	public Gider() { }
	
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
	
	
	public int getOdemeYapildi() {
		return odemeYapildi;
	}

	public void setOdemeYapildi(int odemeYapildi) {
		this.odemeYapildi = odemeYapildi;
	}

	public String getGiderTarihi() {
		return giderTarihi;
	}

	public void setGiderTarihi(String giderTarihi) {
		this.giderTarihi = giderTarihi;
	}

	public int getGiderId() {
		return giderId;
	}

	public void setGiderId(int giderId) {
		this.giderId = giderId;
	}

	public int getElektrik() {
		return elektrik;
	}

	public void setElektrik(int elektrik) {
		this.elektrik = elektrik;
	}

	public int getSu() {
		return su;
	}

	public void setSu(int su) {
		this.su = su;
	}

	public int getDogalgaz() {
		return dogalgaz;
	}

	public void setDogalgaz(int dogalgaz) {
		this.dogalgaz = dogalgaz;
	}

	public int getMaaslar() {
		return maaslar;
	}

	public void setMaaslar(int maaslar) {
		this.maaslar = maaslar;
	}

	public int getYemekGideri() {
		return yemekGideri;
	}

	public void setYemekGideri(int yemekGideri) {
		this.yemekGideri = yemekGideri;
	}
	
}
