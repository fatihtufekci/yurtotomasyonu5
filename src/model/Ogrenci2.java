package model;

public class Ogrenci2 implements Cloneable{
	
	private int ogrenciId;
	private String ad;
	private String soyad;
	private Long tc;
	private Long telefon;
	private Long veliTelefon;
	private int kacinciSinif;
	private Oda oda;
	private int borcVade;
	private YurtFiyat yurtFiyat;
	private String kullaniciAdi;
	private String parola;
	private int kartId;

	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
	
	public Ogrenci2() { }

	public int getOgrenciId() {
		return ogrenciId;
	}

	public void setOgrenciId(int ogrenciId) {
		this.ogrenciId = ogrenciId;
	}

	public String getAd() {
		return ad;
	}

	public void setAd(String ad) {
		this.ad = ad;
	}

	public String getSoyad() {
		return soyad;
	}

	public void setSoyad(String soyad) {
		this.soyad = soyad;
	}

	public Long getTc() {
		return tc;
	}

	public void setTc(Long tc) {
		this.tc = tc;
	}

	public Long getTelefon() {
		return telefon;
	}

	public void setTelefon(Long telefon) {
		this.telefon = telefon;
	}

	public Long getVeliTelefon() {
		return veliTelefon;
	}

	public void setVeliTelefon(Long veliTelefon) {
		this.veliTelefon = veliTelefon;
	}

	public int getKacinciSinif() {
		return kacinciSinif;
	}

	public void setKacinciSinif(int kacinciSinif) {
		this.kacinciSinif = kacinciSinif;
	}

	public Oda getOda() {
		return oda;
	}

	public void setOda(Oda oda) {
		this.oda = oda;
	}

	public int getBorcVade() {
		return borcVade;
	}

	public void setBorcVade(int borcVade) {
		this.borcVade = borcVade;
	}

	public YurtFiyat getYurtFiyat() {
		return yurtFiyat;
	}

	public void setYurtFiyat(YurtFiyat yurtFiyat) {
		this.yurtFiyat = yurtFiyat;
	}

	public String getKullaniciAdi() {
		return kullaniciAdi;
	}

	public void setKullaniciAdi(String kullaniciAdi) {
		this.kullaniciAdi = kullaniciAdi;
	}

	public String getParola() {
		return parola;
	}

	public void setParola(String parola) {
		this.parola = parola;
	}

	public int getKartId() {
		return kartId;
	}

	public void setKartId(int kartId) {
		this.kartId = kartId;
	}
}
