package model;

public class Ogrenci3 implements Cloneable{

	private int ogrenciId;
	private String ad;
	private String soyad;
	private Long tc;
	private Long telefon;
	private Long veliTelefon;
	private int kacinciSinif;
	private Oda oda;
	private Borc borc;
	private String kullaniciAdi;
	private String parola;
	private String kartId;
	
	public Ogrenci3() { }
	
	public Borc getBorc() {
		return borc;
	}
	public void setBorc(Borc borc) {
		this.borc = borc;
	}
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
	public int getOgrenciId() {
		return ogrenciId;
	}
	public void setOgrenciId(int ogrenciId) {
		this.ogrenciId = ogrenciId;
	}
	public String getAd() {
		return ad;
	}
	public void setAd(String ad) {
		this.ad = ad;
	}
	public String getSoyad() {
		return soyad;
	}
	public void setSoyad(String soyad) {
		this.soyad = soyad;
	}
	public Long getTc() {
		return tc;
	}
	public void setTc(Long tc) {
		this.tc = tc;
	}
	public Long getTelefon() {
		return telefon;
	}
	public void setTelefon(Long telefon) {
		this.telefon = telefon;
	}
	public Long getVeliTelefon() {
		return veliTelefon;
	}
	public void setVeliTelefon(Long veliTelefon) {
		this.veliTelefon = veliTelefon;
	}
	public int getKacinciSinif() {
		return kacinciSinif;
	}
	public void setKacinciSinif(int kacinciSinif) {
		this.kacinciSinif = kacinciSinif;
	}
	public Oda getOda() {
		return oda;
	}
	public void setOda(Oda oda) {
		this.oda = oda;
	}
	public String getKullaniciAdi() {
		return kullaniciAdi;
	}
	public void setKullaniciAdi(String kullaniciAdi) {
		this.kullaniciAdi = kullaniciAdi;
	}
	public String getParola() {
		return parola;
	}
	public void setParola(String parola) {
		this.parola = parola;
	}
	public String getKartId() {
		return kartId;
	}
	public void setKartId(String kartId) {
		this.kartId = kartId;
	}
}
