package model;

public class YurtFiyat {
	
	private int yurtFiyatId;
	private int toplamTutar;
	private int odaKapasitesi;
	
	public YurtFiyat() { }

	public int getYurtFiyatId() {
		return yurtFiyatId;
	}

	public void setYurtFiyatId(int yurtFiyatId) {
		this.yurtFiyatId = yurtFiyatId;
	}

	public int getToplamTutar() {
		return toplamTutar;
	}

	public void setToplamTutar(int toplamTutar) {
		this.toplamTutar = toplamTutar;
	}

	public int getOdaKapasitesi() {
		return odaKapasitesi;
	}

	public void setOdaKapasitesi(int odaKapasitesi) {
		this.odaKapasitesi = odaKapasitesi;
	}
	
}
