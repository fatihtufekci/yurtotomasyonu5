package model;

public class Borc{
	
	private int borcId;
	private int borcMiktari;
	private int borcVade;
	private String borcTarihi;
	private YurtFiyat yurtFiyat;
	
	public Borc() { }
	
	
	public String getBorcTarihi() {
		return borcTarihi;
	}

	public void setBorcTarihi(String borcTarihi) {
		this.borcTarihi = borcTarihi;
	}
	
	public int getBorcId() {
		return borcId;
	}

	public void setBorcId(int borcId) {
		this.borcId = borcId;
	}

	public int getBorcMiktari() {
		return borcMiktari;
	}

	public void setBorcMiktari(int borcMiktari) {
		this.borcMiktari = borcMiktari;
	}

	public int getBorcVade() {
		return borcVade;
	}

	public void setBorcVade(int borcVade) {
		this.borcVade = borcVade;
	}

	public YurtFiyat getYurtFiyat() {
		return yurtFiyat;
	}

	public void setYurtFiyat(YurtFiyat yurtFiyat) {
		this.yurtFiyat = yurtFiyat;
	}
	
	
	
}
