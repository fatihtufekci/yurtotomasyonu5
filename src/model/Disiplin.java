package model;

public class Disiplin implements Cloneable{
	
	private int disiplinId;
	private String sebep;
	private String cezasi;
	private int ogrenciId;

	public Disiplin() {}

	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
	
	public int getDisiplinId() {
		return disiplinId;
	}

	public void setDisiplinId(int disiplinId) {
		this.disiplinId = disiplinId;
	}

	public String getSebep() {
		return sebep;
	}

	public void setSebep(String sebep) {
		this.sebep = sebep;
	}

	public String getCezasi() {
		return cezasi;
	}

	public void setCezasi(String cezasi) {
		this.cezasi = cezasi;
	}

	public int getOgrenciId() {
		return ogrenciId;
	}

	public void setOgrenciId(int ogrenciId) {
		this.ogrenciId = ogrenciId;
	} 
	
}
