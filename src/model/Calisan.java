package model;

public class Calisan implements Cloneable{
	
	private int calisanId;
	private Long tc;
	private String ad;
	private String soyad;
	private Long telefon;
	// private int gorevId;
	private Gorev gorev;
	private int maas;
	
	public Calisan() {}
	
	
	public void setGorev(Gorev gorev) {
		this.gorev = gorev;
	}
	public Gorev getGorev() {
		return gorev;
	}
	
	
	public int getCalisanId() {
		return calisanId;
	}
	public void setCalisanId(int calisanId) {
		this.calisanId = calisanId;
	}
	public Long getTc() {
		return tc;
	}
	public void setTc(Long tc) {
		this.tc = tc;
	}
	public String getAd() {
		return ad;
	}
	public void setAd(String ad) {
		this.ad = ad;
	}
	public String getSoyad() {
		return soyad;
	}
	public void setSoyad(String soyad) {
		this.soyad = soyad;
	}
	public Long getTelefon() {
		return telefon;
	}
	public void setTelefon(Long telefon) {
		this.telefon = telefon;
	}
	public int getMaas() {
		return maas;
	}
	public void setMaas(int maas) {
		this.maas = maas;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}
