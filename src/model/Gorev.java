package model;

public class Gorev {
	
	private int gorevId;
	private String gorev;
	
	public Gorev() { }

	public int getGorevId() {
		return gorevId;
	}
	public void setGorevId(int gorevId) {
		this.gorevId = gorevId;
	}
	public String getGorev() {
		return gorev;
	}
	public void setGorev(String gorev) {
		this.gorev = gorev;
	}
	
}
