package model;

public class Oda {
	
	private int odaId;
	private int odaNo;
	private int odaKapasite;
	
	public Oda() { }

	public int getOdaId() {
		return odaId;
	}

	public void setOdaId(int odaId) {
		this.odaId = odaId;
	}

	public int getOdaNo() {
		return odaNo;
	}

	public void setOdaNo(int odaNo) {
		this.odaNo = odaNo;
	}

	public int getOdaKapasite() {
		return odaKapasite;
	}

	public void setOdaKapasite(int odaKapasite) {
		this.odaKapasite = odaKapasite;
	}

}
