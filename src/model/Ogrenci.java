package model;

public class Ogrenci implements Cloneable{
	
	private int ogrenciId;
	private String ad;
	private String soyad;
	private Long tc;
	private Long telefon;
	private Long veliTelefon;
	private int kacinciSinif;
	private int borcMiktari;
	private int odaNo;

	private String kullaniciAdi;
	private String parola;
	
	@Override
	public Object clone() throws CloneNotSupportedException {
		
		return super.clone();
	}
	
	@Override
	public String toString() {
		return "Ogrenci [ad:"+ ad+ ", soyad:"+soyad+ ", tc:"+tc+ ", telefon:"+telefon+ ", veliTelefon:"+veliTelefon+ ", "
				+ "kacinciSinif:"+kacinciSinif+ ", borcMiktari:"+borcMiktari+ ", odaNo:"+odaNo+ ", kullaniciAdi:"+kullaniciAdi;
	}
	
	public Ogrenci() {
	}

	public String getAd() {
		return ad;
	}

	public void setAd(String ad) {
		this.ad = ad;
	}

	public String getSoyad() {
		return soyad;
	}

	public void setSoyad(String soyad) {
		this.soyad = soyad;
	}

	public Long getTc() {
		return tc;
	}

	public void setTc(Long tc) {
		this.tc = tc;
	}

	public Long getTelefon() {
		return telefon;
	}

	public void setTelefon(Long telefon) {
		this.telefon = telefon;
	}

	public Long getVeliTelefon() {
		return veliTelefon;
	}

	public void setVeliTelefon(Long veliTelefon) {
		this.veliTelefon = veliTelefon;
	}

	public int getKacinciSinif() {
		return kacinciSinif;
	}

	public void setKacinciSinif(int kacinciSinif) {
		this.kacinciSinif = kacinciSinif;
	}

	public int getBorcMiktari() {
		return borcMiktari;
	}

	public void setBorcMiktari(int borcMiktari) {
		this.borcMiktari = borcMiktari;
	}

	public int getOgrenciId() {
		return ogrenciId;
	}

	public void setOgrenciId(int ogrenciId) {
		this.ogrenciId = ogrenciId;
	}

	public int getOdaNo() {
		return odaNo;
	}

	public void setOdaNo(int odaNo) {
		this.odaNo = odaNo;
	}

	public String getKullaniciAdi() {
		return kullaniciAdi;
	}

	public void setKullaniciAdi(String kullaniciAdi) {
		this.kullaniciAdi = kullaniciAdi;
	}

	public String getParola() {
		return parola;
	}

	public void setParola(String parola) {
		this.parola = parola;
	}

}
