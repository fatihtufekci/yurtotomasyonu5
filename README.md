     
>    **YURT OTOMASYONU** 


* **Projenin Amacı:** Projemiz yurt sistemini organize ve daha yönetilebilir hale getirmeyi hedeflemektedir. Öğrencilerin yurda giriş çıkış saatlerinden yemekhane giriş çıkış saatlerine kadar detaylı olarak öğrenciyi takip ederek ailesinin de görebilmesini sağlar. Bu proje velinin çocuğunu daha iyi takip etmesini sağlar. 

* **Projenin Faydası:** Yurt yöneticileri için kolay yönetilir bir yurt sistemi sağlar. Gelir ve giderler çok rahat takip edilir. Veliler içinse çocuklarının ne zaman nerede olduğunu bilmelerini sağlayarak içlerinin rahat olmasını sağlar.

* **UseCase**
  (https://gitlab.com/fatihtufekci/yurtotomasyonu5/blob/master/img/usecase.png)

* **Detaylar**
  (https://gitlab.com/fatihtufekci/yurtotomasyonu5/blob/master/img/)
